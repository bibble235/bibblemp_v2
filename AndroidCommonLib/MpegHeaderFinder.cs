﻿using System;
using System.Collections.Generic;

namespace AndroidCommonLib
{
    public class MpegHeaderFinder
    {
        public static byte Byte1 = 0xFF;
        public static byte Byte2 = 0xE0;

        public static int BufferSize = 100;

        public static byte[] FindMpegHeader(ICommonStreamReader stream)
        {
            // Mpeg header to populate
            var myMpegHeader = new byte[4] { 0x0, 0x0, 0x0, 0x0 };

            // Bytes to read
            var myBytes = new byte[BufferSize];

            // Flag to continue in loop
            var myContinue = true;

            // Do initial first read
            var myBytesToProcess = ReadFirstPart(stream);

            do
            {
                if (myBytesToProcess == null || myBytesToProcess.Length < 3)
                    break;

                // Found the mpeg header
                // Copy the bytes and exit
                var myResult = LookForMpegHeaderInBuffer(myBytesToProcess);
                if (myResult >= 0)
                {
                    myMpegHeader[0] = 0xFF;
                    myMpegHeader[1] = myBytesToProcess[myResult + 1];
                    myMpegHeader[2] = myBytesToProcess[myResult + 2];
                    myMpegHeader[3] = myBytesToProcess[myResult + 3];
                    myContinue = false;
                }
                else
                {
                    myBytesToProcess = ReadNextPart(stream, myBytesToProcess);
                }
            }

            // Do flag to continue is true and bytes are available
            while (myContinue && myBytesToProcess != null && myBytesToProcess.Length >= 3);

            return myMpegHeader;
        }

        /// <summary>
        /// We only search upto the buffer length - 4 as it might be split
        /// </summary>
        /// <param name="buffer">buffer to parse</param>
        /// <returns>return either  -1 not found or result of where the characters are</returns>
        private static int LookForMpegHeaderInBuffer(IReadOnlyList<byte> buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));

            if (buffer.Count < 4)
                throw new Exception($"Insufficent buffer to process. Length of buffer = {buffer.Count}.");

            var myResult = -1;

            for (var myIndex = 0; myIndex < buffer.Count - 3; myIndex++)
            {
                if (buffer[myIndex] == Byte1 && buffer[myIndex + 1] > Byte2)
                {
                    // Found so set the result to the index
                    myResult = myIndex;
                    break;
                }
            }

            return myResult;
        }

        private static byte[] ReadFirstPart(ICommonStreamReader stream)
        {
            byte[] myBytesToProcess = null;

            if (stream != null && stream.CanRead)
            {
                var myBytes = new byte[BufferSize];
                var myBytesRead = stream.Read(myBytes, 0, myBytes.Length);
                if (myBytesRead > 0)
                {
                    myBytesToProcess = new byte[myBytesRead];
                    Array.Copy(myBytes, myBytesToProcess, myBytesRead);
                }
            }

            return myBytesToProcess;
        }

        private static byte[] ReadNextPart(ICommonStreamReader stream, byte[] previousBytes)
        {
            byte[] myBytesToProcess = null;

            if (stream != null && stream.CanRead)
            {
                var myBytes = new byte[BufferSize];
                var myBytesRead = stream.Read(myBytes, 0, myBytes.Length);
                if (myBytesRead > 0)
                {
                    myBytesToProcess = new byte[myBytesRead + 4];

                    // Copy the last bytes from previous read
                    Array.Copy(
                        previousBytes,
                        previousBytes.Length - 4,
                        myBytesToProcess,
                        0,
                        4);

                    // Add the new bytes just read
                    Array.Copy(
                        myBytes,
                        0,
                        myBytesToProcess,
                        4,
                        myBytesRead);
                }
            }

            return myBytesToProcess;
        }
    }
}
