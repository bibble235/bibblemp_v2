﻿using System;
using System.IO;

namespace AndroidCommonLib
{
    public interface ICommonStreamReader : IDisposable
    {
        bool CanRead { get; }
        int Read(byte[] bytes,int offset, int numberOfBytes);
        long Seek(int position, SeekOrigin seekOrigin);
        long Length { get;}
        long Position { get; }
    }
}