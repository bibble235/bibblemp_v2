﻿using System.IO;

namespace AndroidCommonLib
{
    public class CommonStreamReader : ICommonStreamReader
    {
        private readonly Stream _inputStream;

        public bool CanRead => _inputStream.CanRead;
        public long Length => _inputStream.Length;
        public long Position => _inputStream.Position;

        public CommonStreamReader(Stream inputStream)
        {
            _inputStream = inputStream;
        }

        public int Read(byte[] bytes, int offset, int numberOfBytes)
        {
            return _inputStream.Read(bytes,offset, numberOfBytes);
        }

        public long Seek(int position, SeekOrigin seekOrigin)
        {
            return  _inputStream.Seek(position, seekOrigin);
        }

        public void Dispose()
        {
            _inputStream?.Dispose();
        }
    }
}