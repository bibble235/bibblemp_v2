﻿using System;

namespace AndroidCommonLib
{
    // ReSharper disable once UnusedMember.Global
    public class TimerHelpers
    {
        /// <summary>
        /// Convert Milliseconds time format Hours:Minutes:Seconds
        /// </summary>
        /// <param name="milliseconds">Milliseconds to convert</param>
        /// <returns>String representation in the format hh:mm:ss</returns>
        public static string MillisecondsToTimer(long milliseconds)
        {
            var myTimeSpan = TimeSpan.FromMilliseconds(milliseconds);

            var myDisplayLength = myTimeSpan.Hours != 0 ?
                $"{myTimeSpan.Hours:D2}:{myTimeSpan.Minutes:D2}:{myTimeSpan.Seconds:D2}" :
                $"{myTimeSpan.Minutes:D2}:{myTimeSpan.Seconds:D2}";

            return myDisplayLength;
        }

        /// <summary>
        /// Calculate Percentage based on duration and total duration
        /// </summary>
        /// <param name="currentDuration">current duration</param>
        /// <param name="totalDuration">current total duration</param>
        /// <returns>Percentage</returns>
        public static int GetProgressPercentage(long currentDuration, long totalDuration)
        {
            return (int)Math.Round(currentDuration / (double)totalDuration * 100);
        }

        /// <summary>
        /// Calculate the milliseconds expended given the progressAsPercentage and duration
        /// </summary>
        /// <param name="progressAsPercentage">Progress as a percentage</param>
        /// <param name="totalDurationInMilliseconds">total duration</param>
        /// <returns>current duration in milliseconds</returns>
        public static int ProgressToTimer(int progressAsPercentage, int totalDurationInMilliseconds)
        {
            if(totalDurationInMilliseconds == 0) throw new Exception("Total Duration cannot be 0.");

            return (int)((double)progressAsPercentage / 100 * totalDurationInMilliseconds);
        }
    }
}