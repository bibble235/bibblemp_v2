﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AndroidCommonLib
{
    public class ByteArrayHelper
    {
        /// <summary>
        /// Compares two byte array to see if they are equal
        /// </summary>
        /// <param name="byteArray1">First Byte Array</param>
        /// <param name="byteArray2">Second Byte Array</param>
        /// <returns>True if the arrays are equal</returns>
        static bool AreEqual(IReadOnlyList<byte> byteArray1, IReadOnlyList<byte> byteArray2)
        {
            if (ReferenceEquals(byteArray1, byteArray2))
                return true;

            if (byteArray1 == null || byteArray2 == null)
                return false;

            if (byteArray1.Count != byteArray2.Count)
                return false;

            return !byteArray1.Where((t, i) => t != byteArray2[i]).Any();
        }

        private static byte[] GetBytesUptoSequence(byte[] bytes, int start, byte[] sequence)
        {
            var sequenceIndex = LocateSequence(bytes, start, bytes.Length - start + 1, sequence);

            if (sequenceIndex == -1) return null;

            var result = new byte[sequenceIndex - start];
            Array.Copy(bytes, start, result, 0, result.Length);
            return result;
        }

        /// <summary>
        /// Locate the index of a sequence of bytes
        /// </summary>
        /// <param name="bytes">byte array to search</param>
        /// <param name="sequence">sequence of bytes to search for</param>
        /// <returns>The start index of the sequence or -1 if the sequence is not found</returns>
        private static int LocateSequence(byte[] bytes, params byte[] sequence)
        {
            return LocateSequence(bytes, 0, bytes.Length, sequence);
        }

        /// <summary>
        /// Locate the index of a sequence of bytes
        /// </summary>
        /// <param name="bytes">byte array to search</param>
        /// <param name="start">start position to start searching</param>
        /// <param name="count">the number of bytes to search</param>
        /// <param name="sequence">sequence of bytes to search for</param>
        /// <returns>The start index of the sequence or -1 if the sequence is not found</returns>
        private static int LocateSequence(byte[] bytes, int start, int count, byte[] sequence)
        {
            var mySequenceIndex = 0;

            var myEndIndex = Math.Min(bytes.Length, start + count);

            for (var myByteIndex = start; myByteIndex < myEndIndex; myByteIndex++)
            {
                if (bytes[myByteIndex] == sequence[mySequenceIndex])
                {
                    mySequenceIndex++;
                    if (mySequenceIndex >= sequence.Length)
                        return myByteIndex - sequence.Length + 1;
                }
                else
                    mySequenceIndex = 0;
            }
            return -1;
        }

        /// <summary>
        /// Locate the indexes of a given sequence of bytes
        /// </summary>
        /// <param name="bytes">bytes to search</param>
        /// <param name="sequence">sequence to search for</param>
        /// <returns>The start index of the sequence or -1 if the sequence is not found</returns>
        internal static int[] LocateSequences(byte[] bytes, params byte[] sequence)
        {
            return LocateSequences(bytes, 0, bytes.Length, sequence);
        }

        /// <summary>
        /// Locate the indexes of a given sequence of bytes
        /// </summary>
        /// <param name="bytes">bytes to search</param>
        /// <param name="start">start position to start searching</param>
        /// <param name="count">the number of bytes to search</param>
        /// <param name="sequence">sequence to search for</param>
        /// <returns>returns list of indexes where sequence was found or an empty array</returns>
        static int[] LocateSequences(byte[] bytes, int start, int count, byte[] sequence)
        {
            var myLocations = new List<int>();

            var mySequenceLocation = LocateSequence(bytes, start, count, sequence);
            while (mySequenceLocation >= 0)
            {
                myLocations.Add(mySequenceLocation);
                count -= mySequenceLocation - start + 1;
                start = mySequenceLocation + sequence.Length;
                mySequenceLocation = LocateSequence(bytes, start, count, sequence);
            }

            return myLocations.ToArray();
        }

        /// <summary>
        /// Split a byte array into pieces based on a sequence
        /// </summary>
        /// <param name="bytes">bytes to split</param>
        /// <param name="sequence">sequence to split on</param>
        /// <returns>A list of bytes between the sequence</returns>
        private static byte[][] SplitBySequence(byte[] bytes, params byte[] sequence)
        {
            return SplitBySequence(bytes, 0, bytes.Length, sequence);
        }

        /// <summary>
        /// Split a byte array into pieces based on a sequence
        /// </summary>
        /// <param name="bytes">bytes to split</param>
        /// <param name="start">start position to start searching</param>
        /// <param name="count">the number of bytes to search</param>
        /// <param name="sequence">sequence to split on</param>
        /// <returns>A list of bytes between the sequence</returns>
        public static byte[][] SplitBySequence(byte[] bytes, int start, int count, byte[] sequence)
        {
            if (start + count > bytes.Length)
                count = bytes.Length - start;

            var myLocations = LocateSequences(bytes, start, count, sequence) ?? throw new ArgumentNullException("LocateSequences(bytes, start, count, sequence)");

            if (myLocations.Length == 0)
                return new[] { bytes };

            var myResults = new List<byte[]>(myLocations.Length + 1);

            for (var locationIdx = 0; locationIdx < myLocations.Length; locationIdx++)
            {
                var myStartIndex = locationIdx > 0 ? myLocations[locationIdx - 1] + sequence.Length : start;
                var myEndIndex = myLocations[locationIdx] - 1;

                if (myEndIndex < myStartIndex)
                {
                    myResults.Add(new byte[0]);
                }
                else
                {
                    var splitBytes = new byte[myEndIndex - myStartIndex + 1];
                    Array.Copy(bytes, myStartIndex, splitBytes, 0, splitBytes.Length);
                    myResults.Add(splitBytes);
                }
            }

            if (myLocations[myLocations.Length - 1] + sequence.Length > start + count - 1)
                myResults.Add(new byte[0]);
            else
            {
                var splitBytes = new byte[start + count - myLocations[myLocations.Length - 1] - sequence.Length];
                Array.Copy(bytes, myLocations[myLocations.Length - 1] + sequence.Length, splitBytes, 0, splitBytes.Length);
                myResults.Add(splitBytes);
            }

            return myResults.ToArray();
        }
    }
}