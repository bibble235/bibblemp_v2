﻿using System;

namespace AndroidCommonLib
{
    public class BitReader
    {
        public static int ToIntShift7(byte[] bytes, int startIndex, int numberOfBytes)
        {

            return ToIntShift(bytes, startIndex, numberOfBytes, 7);
        }
        public static int ToIntShift8(byte[] bytes, int startIndex, int numberOfBytes)
        {
            return ToIntShift(bytes, startIndex, numberOfBytes, 8);
        }

        public static int ToIntShift(byte[] bytes, int startIndex, int numberOfBytes, int shiftNumberOfBytes)
        {
            int mySize = 0, shift = 0;
            for (var myByteIndex = startIndex + numberOfBytes - 1; myByteIndex >= startIndex; myByteIndex--)
            {
                mySize += bytes[myByteIndex] << shift;
                shift += shiftNumberOfBytes;
            }
            return mySize;
        }

        public static int ToInt(byte[] bytes, int startIndex, int numberOfBytes, int shiftNumberOfBytes)
        {
            return Convert.ToInt32(bytes);
        }
    }
}