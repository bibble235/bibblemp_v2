﻿using System;
using System.IO;

namespace AndroidCommonLib
{
    public sealed class TemporaryFile : IDisposable
    {
        static readonly string TAG = typeof(TemporaryFile).FullName;
        string _path;

        public TemporaryFile() : this(System.IO.Path.GetTempFileName())
        {
        }

        public TemporaryFile(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));

            _path = path;
        }

        public string Path
        {
            get
            {
                if (_path == null) throw new ObjectDisposedException(GetType().Name);
                return _path;
            }
        }

        ~TemporaryFile()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                GC.SuppressFinalize(this);
            }

            if (_path == null) return;

            try
            {
                File.Delete(_path);
            }
            catch
            {
                // ignored
            }

            _path = null;
        }
    }
}