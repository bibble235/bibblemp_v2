﻿using Mp3InfoLib;
using SambaLib;

namespace SambaMp3Lib
{
    public class SambaMp3Info
    {
        public static Mp3Id3V2Info GetMp3Id3V2Info(string sambaFilename)
        {
            Mp3Id3V2Info myId3V2Info = null;

            var mySambaFileObject = new SambaFileObject(sambaFilename);

            using (var mySambaStreamFileReader = new SambaFileReader(mySambaFileObject))
            {
                var myResult = mySambaStreamFileReader.OpenSambaFile();
                if (myResult)
                {
                    myId3V2Info = new Mp3Id3V2Info(mySambaStreamFileReader);
                    myId3V2Info.ReadTag();
                }
            }
            return myId3V2Info;
        }
    }
}
