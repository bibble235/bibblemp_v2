﻿using System;
using AndroidCommonLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonLibTest
{
    [TestClass]
    public class TimerHelpersUnitTest
    {
        [TestMethod]
        public void MillisecondsToTimer_Test()
        {
            var myTest1 = TimerHelpers.MillisecondsToTimer(1000);
            Assert.IsTrue(myTest1 == "00:01");

            var myTest2 = TimerHelpers.MillisecondsToTimer(10000);
            Assert.IsTrue(myTest2 == "00:10");

            var myTest3 = TimerHelpers.MillisecondsToTimer(600000);
            Assert.IsTrue(myTest3 == "10:00");

            var myTest4 = TimerHelpers.MillisecondsToTimer(3600000);
            Assert.IsTrue(myTest4 == "01:00:00");

            var myTest5 = TimerHelpers.MillisecondsToTimer(36000000);
            Assert.IsTrue(myTest5 == "10:00:00");

            var myTest6 = TimerHelpers.MillisecondsToTimer(35999999);
            Assert.IsTrue(myTest6 == "09:59:59");
        }
        [TestMethod]
        public void GetProgressPercentage_Test()
        {
            var myTest1 = TimerHelpers.GetProgressPercentage(1000, 2000);
            Assert.IsTrue(myTest1 == 50);

            TimerHelpers.GetProgressPercentage(100, 200);
            Assert.IsTrue(myTest1 == 50);

            var myTest3 = TimerHelpers.GetProgressPercentage(1, 100);
            Assert.IsTrue(myTest3 == 1);

            var myTest4 = TimerHelpers.GetProgressPercentage(100, 100);
            Assert.IsTrue(myTest4 == 100);

        }

        [TestMethod]
        public void ProgressToTimer_Test()
        {
            var myTest1 = TimerHelpers.ProgressToTimer(50, 2000);
            Assert.IsTrue(myTest1 == 1000);

            var myTest2 = TimerHelpers.ProgressToTimer(50, 200);
            Assert.IsTrue(myTest2 == 100);

            var myTest3 = TimerHelpers.ProgressToTimer(1, 100);
            Assert.IsTrue(myTest3 == 1);

            var myTest4 = TimerHelpers.ProgressToTimer(100, 100);
            Assert.IsTrue(myTest4 == 100);


            try
            {
                TimerHelpers.ProgressToTimer(100, 0);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("Total Duration cannot be 0."));
            }
        }

    }
}
