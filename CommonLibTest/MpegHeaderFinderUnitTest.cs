using System;
using System.IO;
using System.Linq;
using System.Text;
using AndroidCommonLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonLibTest
{
    [TestClass]
    public class MpegHeaderFinderUnitTest
    {
        [TestMethod]
        public void MpegHeaderFinderUnitTest_EmptyStream()
        {
            ICommonStreamReader myStream = null;

            var myResult = MpegHeaderFinder.FindMpegHeader(myStream);
            Assert.IsTrue(myResult[0] == 0 &&
                 myResult[1] == 0 &&
                 myResult[2] == 0 &&
                 myResult[3] == 0);
        }
        [TestMethod]
        public void MpegHeaderFinderUnitTest_TooShort()
        {
            var myStream = GenerateStreamFromString("AAA");

            byte[] myResult = null;

            try
            {
                myResult = MpegHeaderFinder.FindMpegHeader(myStream);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("Insufficent buffer to process. Length of buffer = 3."));
            }
        }

        [TestMethod]
        public void MpegHeaderFinderUnitTest_FoundAtBeginning()
        {
            var myBitHeader = new byte[] { 0xFF, 0xE1, 0x41, 0x41 };
            var myByteArray = Enumerable.Repeat((byte)0x20, 200).ToArray();
            Array.Copy(myBitHeader, 0, myByteArray, 0, 4);

            var myMemoryStream = new MemoryStream();
            myMemoryStream.Write(myByteArray, 0, myByteArray.Length);
            myMemoryStream.Seek(0, SeekOrigin.Begin);

            ICommonStreamReader myStream = new CommonStreamReader(myMemoryStream);

            byte[] myResult;

            myResult = MpegHeaderFinder.FindMpegHeader(myStream);
            Assert.IsTrue(myResult[0] == 0xFF &&
                          myResult[1] == 0xE1 &&
                          myResult[2] == 0x41 &&
                          myResult[3] == 0x41);
        }

        [TestMethod]
        public void MpegHeaderFinderUnitTest_FoundInMiddle()
        {
            var myBitHeader = new byte[] { 0xFF, 0xE1, 0x41, 0x41 };
            var myByteArray = Enumerable.Repeat((byte)0x20, 200).ToArray();
            Array.Copy(myBitHeader, 0, myByteArray, 98, 4);

            var myMemoryStream = new MemoryStream();
            myMemoryStream.Write(myByteArray, 0, myByteArray.Length);
            myMemoryStream.Seek(0, SeekOrigin.Begin);

            ICommonStreamReader myStream = new CommonStreamReader(myMemoryStream);

            byte[] myResult;

            myResult = MpegHeaderFinder.FindMpegHeader(myStream);
            Assert.IsTrue(myResult[0] == 0xFF &&
                          myResult[1] == 0xE1 &&
                          myResult[2] == 0x41 &&
                          myResult[3] == 0x41);
        }

        [TestMethod]
        public void MpegHeaderFinderUnitTest_NotFound()
        {
            var myByteArray = Enumerable.Repeat((byte)0x20, 200).ToArray();

            var myMemoryStream = new MemoryStream();
            myMemoryStream.Write(myByteArray, 0, myByteArray.Length);

            ICommonStreamReader myStream = new CommonStreamReader(myMemoryStream);

            byte[] myResult;

            myResult = MpegHeaderFinder.FindMpegHeader(myStream);
            Assert.IsTrue(myResult[0] == 0x0 &&
                          myResult[1] == 0x0 &&
                          myResult[2] == 0x0 &&
                          myResult[3] == 0x0);
        }

        [TestMethod]
        public void MpegHeaderFinderUnitTest_FoundAtEnd()
        {
            var myBitHeader = new byte[] { 0xFF, 0xE1, 0x41, 0x41 };
            var myByteArray = Enumerable.Repeat((byte)0x20, 200).ToArray();
            Array.Copy(myBitHeader, 0, myByteArray, 196, 4);

            var myMemoryStream = new MemoryStream();
            myMemoryStream.Write(myByteArray, 0, myByteArray.Length);
            myMemoryStream.Seek(0, SeekOrigin.Begin);

            ICommonStreamReader myStream = new CommonStreamReader(myMemoryStream);
            
            byte[] myResult;

            myResult = MpegHeaderFinder.FindMpegHeader(myStream);
            Assert.IsTrue(myResult[0] == 0xFF &&
                          myResult[1] == 0xE1 &&
                          myResult[2] == 0x41 &&
                          myResult[3] == 0x41);
        }

        [TestMethod]
        public void MpegHeaderFinderUnitTest_Almost_Found()
        {
            var myByteArray = new byte[] { 0x41, 0x41, 0x41, 0x41, 0x41, 0xFF, 0xE1, 0x41 };
            var myMemoryStream = new MemoryStream();
            myMemoryStream.Write(myByteArray,0,myByteArray.Length);

            ICommonStreamReader myStream = new CommonStreamReader(myMemoryStream);

            byte[] myResult = null;

            myResult = MpegHeaderFinder.FindMpegHeader(myStream);
            Assert.IsTrue(myResult[0] == 0x0 &&
                          myResult[1] == 0x0 &&
                          myResult[2] == 0x0 &&
                          myResult[3] == 0x0);
        }

        public static ICommonStreamReader GenerateStreamFromString(string s)
        {
            var myBytes = Encoding.ASCII.GetBytes(s);

            var myMemoryStream = new MemoryStream();
            myMemoryStream.Write(myBytes,0,myBytes.Length);

            ICommonStreamReader myStream = new CommonStreamReader(myMemoryStream);

            myStream.Seek(0,SeekOrigin.Begin);
            return myStream;
        }
    }
}
