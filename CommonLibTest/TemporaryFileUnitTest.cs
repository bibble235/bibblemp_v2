﻿using System.IO;
using AndroidCommonLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonLibTest
{
    [TestClass]
    public class TemporaryFileUnitTest
    {
        [TestMethod]
        public void TemporaryTest_Test()
        {
            var myFilename = "";
            using (var myFile = new TemporaryFile())
            {
                myFilename = myFile.Path;
            }

            var myResult = File.Exists(myFilename);
            Assert.IsTrue(myResult == false);
        }
    }
}
