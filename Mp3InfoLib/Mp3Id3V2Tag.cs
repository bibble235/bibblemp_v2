﻿
namespace Mp3InfoLib
{
    public class Mp3Id3V2Tag
    {
        public enum Id3TextEncoding : byte
        {
            Iso8859_1 = 0,
            Unicode = 1
        }

        public Id3TextEncoding TextEncoding { get; set; }
        public string Tag { get; set; }
        public string Value { get; set; }

        public Mp3Id3V2Tag()
        {
            Tag = string.Empty;
            Value = string.Empty;
            TextEncoding = Id3TextEncoding.Iso8859_1;
        }

        public Mp3Id3V2Tag(string tag, Id3TextEncoding textEncoding, string value)
        {
            Tag = tag;
            Value = value;
            TextEncoding = textEncoding;
        }
    }
}