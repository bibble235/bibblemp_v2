﻿using System.Collections.Generic;

namespace Mp3InfoLib
{
    public class Mp3Header
    {
        private const int VariableFrames = -1;               // FIXME
        private readonly bool HasVariableRateHeader = false; // FIXME

        public long FileSize;
        public byte[] MpegHeader;

        public ulong BitRateHeader;
        public int FrequencyIndex => (int)((BitRateHeader >> 10) & 3);
        public int VersionIndex => (int)((BitRateHeader >> 19) & 3);
        public int LayerIndex => (int)((BitRateHeader >> 17) & 3);
        public int BitRateIndex => (int)((BitRateHeader >> 12) & 15);
        public int FrameSync => (int)((BitRateHeader >> 21) & 2047);
        public int EmphasisIndex => (int)(BitRateHeader & 3);

        private int NumberOfFrames
        {
            get
            {
                // Again, the number of MPEG frames is dependent on whether it's a variable bitrate MP3 or not
                if (!HasVariableRateHeader)
                {
                    var myMediaFrameSize = ((LayerIndex == 3) ? 12 : 144) *
                                           ((1000.0 * BitRate) / Frequency);
                    return (int)(FileSize / myMediaFrameSize);
                }

                return VariableFrames;
            }
        }
        public int Frequency
        {
            get
            {
                int[,] table =
                {
                    {32000, 16000, 8000}, // MPEG 2.5
                    {0, 0, 0}, // reserved
                    {22050, 24000, 16000}, // MPEG 2
                    {44100, 48000, 32000} // MPEG 1
                };

                return table[VersionIndex, FrequencyIndex];
            }
        }

        public int BitRate
        {
            get
            {
                // If the file has a variable bitrate, then we return an integer average bitrate,
                // otherwise, we use a lookup table to return the bitrate
                if (HasVariableRateHeader)
                {
                    var medFrameSize = (double)FileSize / NumberOfFrames;
                    return (int)((medFrameSize * Frequency) / (1000.0 * ((LayerIndex == 3) ? 12.0 : 144.0)));
                }
                else
                {
                    int[,,] table =
                    {
                        {
                            // MPEG 2 & 2.5
                            {0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160, 0}, // Layer III
                            {0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160, 0}, // Layer II
                            {0, 32, 48, 56, 64, 80, 96, 112, 128, 144, 160, 176, 192, 224, 256, 0} // Layer I
                        },
                        {
                            // MPEG 1
                            {0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 0}, // Layer III
                            {0, 32, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384, 0}, // Layer II
                            {0, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448, 0} // Layer I
                        }
                    };

                    // var myVersionIndex = VersionIndex;
                    // var myLayerIndex = LayerIndex;
                    // var myBitRateIndex = BitRateIndex;

                    // Console.WriteLine($"VersionIndex = {VersionIndex}, LayerIndex = {LayerIndex}, BitRateIndex = {BitRateIndex}");
                    return table[VersionIndex & 1, LayerIndex - 1, BitRateIndex];
                }
            }
        }

        public Mp3Header(long fileSize, byte[] mpegHeader)
        {
            FileSize = fileSize;
            MpegHeader = mpegHeader;
            BitRateHeader = GetBitRateHeader(mpegHeader);
        }
        private static ulong GetBitRateHeader(IReadOnlyList<byte> c)
        {
            // this thing is quite interesting, it works like the following
            // c[0] = 00000011
            // c[1] = 00001100
            // c[2] = 00110000
            // c[3] = 11000000
            // the operator << means that we'll move the bits in that direction
            // 00000011 << 24 = 00000011000000000000000000000000
            // 00001100 << 16 =         000011000000000000000000
            // 00110000 << 24 =                 0011000000000000
            // 11000000       =                         11000000
            //                +_________________________________
            //                  00000011000011000011000011000000
            return (ulong)(((c[0] & 255) << 24) | ((c[1] & 255) << 16) | ((c[2] & 255) << 8) | ((c[3] & 255)));
        }
        
        /// <summary>
        /// Check the Mp3 Header is sane.
        /// </summary>
        /// <returns>True if valid, false if invalid</returns>
        public bool IsValidHeader()
        {
            return (((FrameSync & 2047) == 2047) &&
                    ((VersionIndex & 3) != 1) &&
                    ((LayerIndex & 3) != 0) &&
                    ((BitRateIndex & 15) != 0) &&
                    ((BitRateIndex & 15) != 15) &&
                    ((FrequencyIndex & 3) != 3) &&
                    ((EmphasisIndex & 3) != 2));
        }

    }
}