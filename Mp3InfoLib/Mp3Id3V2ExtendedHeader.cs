﻿using System;

namespace Mp3InfoLib
{
    public class Mp3Id3V2ExtendedHeader
    {
        public int Crc32 { get; set; }
        public int PaddingSize { get; set; }
    }
}
