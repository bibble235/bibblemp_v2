﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using AndroidCommonLib;
using DataLib;

namespace Mp3InfoLib
{
    public class Mp3Id3V2Info
    {
        public enum Id3TagIndicatorTypes
        {
            Id3TagIndicatorFound,
            Id3TagIndicatorNotFound
        }

        public enum Id3ResultTypes
        {
            Id3ResultUnknown,
            Id3ResultFileOk,
            Id3ResultIncorrectFormat,
            Id3ResultMpegHeaderNotFound,
            Id3ResultMpegHeaderNotValid,
            Id3ResultFlac,
        }

        public Id3TagIndicatorTypes Id3TagIndicator { get; set; }
        public Id3ResultTypes Id3Result { get; set; }

        public int _MajorVersion;
        public int _MinorVersion;

        public Mp3Id3V2Header Id3v2Header { get; private set; }
        public TrackLength TrackLength { get; private set; }
        public MemoryStream ArtImage { get; private set; }

        public string TrackName { get; private set; }
        private ICommonStreamReader StreamFileReader { get; }

        public Mp3Id3V2Info(ICommonStreamReader streamFileReader)
        {
            StreamFileReader = streamFileReader;
            Id3TagIndicator = Id3TagIndicatorTypes.Id3TagIndicatorNotFound;
        }

        /// <summary>
        /// Read the Tags for the file
        /// </summary>
        public void ReadTag()
        {
            ReadStandardHeader();
            if (Id3TagIndicator == Id3TagIndicatorTypes.Id3TagIndicatorFound)
            {
                ReadHeader();

                if (TrackLength == null || TrackLength.Length == 0)
                {
                    ReadMpeg();
                }
            }
        }
        private void ReadHeader()
        {
            // Set current position to 
            StreamFileReader.Seek(4, SeekOrigin.Begin);

            // Read Six bytes from position 4
            var myHeaderBytes = new byte[6];
            StreamFileReader.Read(myHeaderBytes, 0, 6);

            // Create  Header
            byte myFlags = myHeaderBytes[1];
            Id3v2Header = new Mp3Id3V2Header()
            {
                Header = new Mp3Id3V2StandardHeader
                {
                    Revision = myHeaderBytes[0],
                    Unsyncronization = (myFlags & 0x80) > 0,
                    ExtendedHeader = (myFlags & 0x40) > 0,
                    Experimental = (myFlags & 0x20) > 0
                },
            };

            // Read Tag size
            var myTagSize = BitReader.ToIntShift7(myHeaderBytes, 2, 4);

            // Read Tag Data
            // Console.WriteLine($"Header size {myTagSize}");

            var myTagData = new byte[myTagSize];
            StreamFileReader.Read(myTagData, 0, myTagSize);

            // Console.WriteLine($"Current Memory Position = {StreamFileReader.MemoryStream.Position}");
            var myCurrentPosition = 0;
            if (Id3v2Header.Header.ExtendedHeader)
            {
                Id3v2Header.ExtendedHeader = ReadExtendedHeader(ref myCurrentPosition, myTagData);
            }

            ReadTagData(myCurrentPosition, myTagData);
        }

        private Mp3Id3V2ExtendedHeader ReadExtendedHeader(ref int myCurrentPosition, byte[] myTagData)
        {
            BitReader.ToIntShift7(myTagData, myCurrentPosition, 4);
            myCurrentPosition += 4;

            var myExtendedHeader = new Mp3Id3V2ExtendedHeader
            {
                PaddingSize = BitReader.ToIntShift8(myTagData, myCurrentPosition + 2, 4)
            };

            if ((myTagData[myCurrentPosition] & 0x80) > 0)
            {
                myExtendedHeader.Crc32 = BitReader.ToIntShift7(myTagData, myCurrentPosition + 6, 4);
                myCurrentPosition += 10;
            }
            else
            {
                myCurrentPosition += 6;
            }

            return myExtendedHeader;
        }

        /// <summary>
        /// Read all of the tags looking for the Track Name and the Track Duration
        /// </summary>
        /// <param name="myCurrentPosition">Current Position in the tag data to start from</param>
        /// <param name="myTagData">Tag Data to read</param>
        private void ReadTagData(int myCurrentPosition, byte[] myTagData)
        {
            var myTagSize = myTagData.Length;

            while (myCurrentPosition < myTagSize && myTagData[myCurrentPosition] != 0x00)
            {
                var myFrameId = Encoding.ASCII.GetString(myTagData, myCurrentPosition, 4);
                myCurrentPosition += 4;

                // System.OutOfMemoryException myException can occur with exact audio copy.
                // If it does we need to re-read the tag and re-allocate
                if (myFrameId[0] == '?')
                {
                    var myFudgeFramePosition = myCurrentPosition - 3;
                    myFrameId = Encoding.ASCII.GetString(myTagData, myFudgeFramePosition, 4);
                    myCurrentPosition += 1;
                }

                var myFrameSize = BitReader.ToIntShift8(myTagData, myCurrentPosition, 4);
                myCurrentPosition += 4;

                var myFrameFlags = (ushort)((myTagData[myCurrentPosition] << 0x08) + myTagData[myCurrentPosition + 1]);
                myCurrentPosition += 2;

                // We need to track the file size against the available copies of data
                // Console.WriteLine($"{myFrameId}, Current Position {myCurrentPosition} Allocating {myFrameSize} for Frame Id {myFrameId}, next position {myCurrentPosition + 10 + myFrameSize}");
                var myFrameData = new byte[myFrameSize];

                Array.Copy(myTagData, myCurrentPosition, myFrameData, 0, myFrameSize);

                if (myFrameId == "TLEN")
                {
                    var myIdv3Tag = Mp3FrameHandler.HandleTag(myFrameId, myFrameData);
                    TrackLength = new TrackLength(Convert.ToInt32(myIdv3Tag.Value), TrackLength.DurationUnits.Milliseconds);
                    Console.WriteLine($"Found {myIdv3Tag.Value}");
                }
                else if (myFrameId == "APIC")
                {
                    ProcessAPIC(myFrameData);
                }
                else if (myFrameId == "TIT2")
                {
                    var myIdv3Tag = Mp3FrameHandler.HandleTag(myFrameId, myFrameData);
                    TrackName = myIdv3Tag.Value;
                }

                // Got everything we want so exiting
                if (!string.IsNullOrEmpty(TrackName) && TrackLength != null && TrackLength.Length > 0)
                {
                    Id3Result = Id3ResultTypes.Id3ResultFileOk;
                    break;
                }

                myCurrentPosition += myFrameSize;
            }
        }

        private void ProcessAPIC(byte[] frameData)
        {
            var tmpStart = new byte[40];

            Array.Copy(frameData, tmpStart,Math.Min(40, frameData.Length));

            var tagContent = Encoding.ASCII.GetString(tmpStart);

            int zeroCount = 0, ii = tagContent.IndexOf("image/");
            while (zeroCount < 3)
            {
                if (frameData[ii] == 0)
                {
                    zeroCount++;
                }
                ii++;
            }

            tagContent = tagContent.Remove(0,
                tagContent.IndexOf("image/", StringComparison.Ordinal) + 6);

            var imgExt = tagContent.Substring(0,
                tagContent.IndexOf('\0'));

            if ((frameData.Length - ii) > 0)
            {
                ArtImage = new MemoryStream();
                ArtImage.Write(frameData, ii, frameData.Length - ii);
                ArtImage.Close();
            }
        }

        private void ReadMpeg()
        {
            var myBitRateHeaderBytes = MpegHeaderFinder.FindMpegHeader(StreamFileReader);
            if (myBitRateHeaderBytes[0] != 0x0)
            {
                var myMp3Header = new Mp3Header(StreamFileReader.Length, myBitRateHeaderBytes);
                if (myMp3Header.IsValidHeader())
                {
                    ProcessMpegHeader(myMp3Header.BitRate);
                }
                else
                {
                    Id3Result = Id3ResultTypes.Id3ResultMpegHeaderNotValid;
                }
            }
            else
            {
                Id3Result = Id3ResultTypes.Id3ResultMpegHeaderNotFound;
            }
        }

        private void ProcessMpegHeader(int bitRate)
        {
            var myFileLength = StreamFileReader.Length -
                               StreamFileReader.Position;

            var myKiloBitFileSize = (int)((8 * myFileLength) / 1000);

            var myLengthInSeconds = myKiloBitFileSize / bitRate;

            TrackLength = new TrackLength(
                Convert.ToInt32(myLengthInSeconds),
                TrackLength.DurationUnits.Seconds);

            Id3Result = Id3ResultTypes.Id3ResultFileOk;

        }

        /// <summary>
        /// Read the standard header
        /// Check to make sure it is not flac
        /// 
        /// Read header
        /// 0-2 ID3
        /// 3   Major Version
        /// 4   Minor Version
        /// 
        /// </summary>
        private void ReadStandardHeader()
        {
            var myStandardHeaderBytes = new byte[5];

            StreamFileReader.Read(myStandardHeaderBytes, 0, 5);

            var myFoundFLAC = CheckForFlac(myStandardHeaderBytes);

            if (myFoundFLAC)
            {
                // Console.WriteLine("Found FLAC indicator so exiting");
                Id3Result = Id3ResultTypes.Id3ResultFlac;
                return;
            }

            var myMagic = Encoding.ASCII.GetString(myStandardHeaderBytes, 0, 3);
            if (myMagic != "ID3" || myStandardHeaderBytes[3] != 3)
            {
                Id3Result = Id3ResultTypes.Id3ResultIncorrectFormat;
                return;
            }

            Id3TagIndicator = Id3TagIndicatorTypes.Id3TagIndicatorFound;

            _MajorVersion = Convert.ToInt32(myStandardHeaderBytes[3]);
            _MinorVersion = Convert.ToInt32(myStandardHeaderBytes[4]);
            // Console.WriteLine($"ID3 is Major Version {_MajorVersion}, Minor Version{_MinorVersion}");
        }

        /// <summary>
        /// Reads the first four bytes and searches for fLaC as per spec
        /// https://xiph.org/flac/format.html
        /// </summary>
        /// <param name="firstFourBytesOfFile">First four bytes of file</param>
        /// <returns></returns>
        private static bool CheckForFlac(IReadOnlyList<byte> firstFourBytesOfFile)
        {
            var myFoundFlac = firstFourBytesOfFile[0] == 0x66 &&
                               firstFourBytesOfFile[1] == 0x4C &&
                               firstFourBytesOfFile[2] == 0x61 &&
                               firstFourBytesOfFile[3] == 0x43;
            return myFoundFlac;
        }

    }
}