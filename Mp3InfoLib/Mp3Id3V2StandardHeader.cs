﻿namespace Mp3InfoLib
{
    public class Mp3Id3V2StandardHeader
    {
        public bool Experimental { get; set; }
        public bool ExtendedHeader { get; set; }
        public int Revision { get; set; }
        public bool Unsyncronization { get; set; }
    }
}