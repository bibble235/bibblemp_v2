﻿using System.IO;
using AndroidCommonLib;

namespace Mp3InfoLib
{
    public class Mp3Info
    {
        public static Mp3Id3V2Info GetMp3Id3V2Info(string filename)
        {
            Mp3Id3V2Info myId3V2Info = null;
            using (var myStream = new StreamReader(filename))
            {
                using (var myStreamReader = new CommonStreamReader(myStream.BaseStream))
                {
                    myId3V2Info = new Mp3Id3V2Info(myStreamReader);
                    myId3V2Info.ReadTag();
                }
            }

            return myId3V2Info;
        }
    }
}