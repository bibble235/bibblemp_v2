﻿using System.Text;
using AndroidCommonLib;

namespace Mp3InfoLib
{
    public class Mp3TextEncodingHelper
    {
        //Gets the default encoding, which is ISO-8859-1
        internal static Encoding GetDefaultEncoding()
        {
            return Encoding.GetEncoding("iso-8859-1");
        }

        internal static string GetDefaultString(byte[] bytes, int start, int count)
        {
            return GetDefaultEncoding().GetString(bytes, start, count);
        }

        internal static Encoding GetEncoding(Mp3Id3V2Tag.Id3TextEncoding encodingType)
        {
            if (encodingType == Mp3Id3V2Tag.Id3TextEncoding.Iso8859_1)
                return Encoding.GetEncoding("iso-8859-1");
            if (encodingType == Mp3Id3V2Tag.Id3TextEncoding.Unicode)
                return Encoding.Unicode;
            return null;
        }

        internal static string GetString(byte[] bytes, int start, int count, Mp3Id3V2Tag.Id3TextEncoding encodingType)
        {
            Encoding encoding = GetEncoding(encodingType);
            string str = encoding.GetString(bytes, start, count);

            if (encodingType == Mp3Id3V2Tag.Id3TextEncoding.Unicode)
            {
                if (str[0] == '\xFFFE' || str[0] == '\xFEFF')
                    str = str.Remove(0, 1);
            }

            return str;
        }

        internal static string[] GetSplitStrings(byte[] bytes, int start, int count, Mp3Id3V2Tag.Id3TextEncoding encodingType)
        {
            byte[][] splitBytes = ByteArrayHelper.SplitBySequence(bytes, start, count, GetSplitterBytes(encodingType));
            if (splitBytes.Length == 0)
                return new[] { string.Empty };

            var strings = new string[splitBytes.Length];
            for (int splitByteIdx = 0; splitByteIdx < splitBytes.Length; splitByteIdx++)
                strings[splitByteIdx] = GetString(splitBytes[splitByteIdx], 0, splitBytes[splitByteIdx].Length, encodingType);
            return strings;
        }

        internal static byte[] GetSplitterBytes(Mp3Id3V2Tag.Id3TextEncoding encodingType)
        {
            var splitterBytes = new byte[GetSplitterLength(encodingType)];
            return splitterBytes;
        }

        private static int GetSplitterLength(Mp3Id3V2Tag.Id3TextEncoding encodingType)
        {
            if (encodingType == Mp3Id3V2Tag.Id3TextEncoding.Iso8859_1)
                return 1;

            if (encodingType == Mp3Id3V2Tag.Id3TextEncoding.Unicode)
                return 2;
            return -1;
        }
    }
}