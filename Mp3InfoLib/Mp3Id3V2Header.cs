﻿namespace Mp3InfoLib
{
    public class Mp3Id3V2Header
    {
        public Mp3Id3V2StandardHeader Header { get; internal set; }
        public Mp3Id3V2ExtendedHeader ExtendedHeader { get; internal set; }
    }
}