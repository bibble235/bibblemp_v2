﻿using System.Text;

namespace Mp3InfoLib
{
    public class Mp3FrameHandler
    {
        public static Mp3Id3V2Tag HandleTag(string tag, byte[] data)
        {
            var myFrame = new Mp3Id3V2Tag();

            var encodingByte = data[0];

            string myFrameValue;

            if (encodingByte == 0 || encodingByte == 1)
            {
                myFrame.TextEncoding = (Mp3Id3V2Tag.Id3TextEncoding)encodingByte;

                var myEncoding = Mp3TextEncodingHelper.GetEncoding(myFrame.TextEncoding);

                myFrameValue = myEncoding.GetString(data, 1, data.Length - 1);
                if (myFrameValue.Length > 0 &&
                    myFrame.TextEncoding == Mp3Id3V2Tag.Id3TextEncoding.Unicode &&
                    (myFrameValue[0] == '\xFFFE' || myFrameValue[0] == '\xFEFF'))
                {
                    myFrameValue = myFrameValue.Remove(0, 1);
                }
            }
            else
            {
                myFrame.TextEncoding = Mp3Id3V2Tag.Id3TextEncoding.Iso8859_1;
                Encoding encoding = Mp3TextEncodingHelper.GetEncoding(myFrame.TextEncoding);
                myFrameValue = encoding.GetString(data, 0, data.Length);
            }

            myFrame.Value = myFrameValue;
            myFrame.Tag = tag;

            return myFrame;
        }
    }
}