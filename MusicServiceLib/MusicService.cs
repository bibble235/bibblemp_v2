﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.Media.Session;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Media;
using Android.Support.V4.Media.Session;
using Android.Util;
using DataLib;
using Mp3InfoLib;
using SambaLib;

namespace MusicServiceLib
{
    [Service(Exported = true, Name = "nz.co.bibble.MusicService")]
    public class MusicService : Service,
        MediaPlayer.IOnCompletionListener,
        MediaPlayer.IOnErrorListener,
        MediaPlayer.IOnPreparedListener,
        AudioManager.IOnAudioFocusChangeListener
    {
        // ReSharper disable once ArrangeTypeMemberModifiers
        // ReSharper disable once InconsistentNaming
        static readonly string TAG = typeof(MusicService).FullName;

        //AudioPlayer notification ID
        private static readonly int NotificationId = 101;

        public enum PlaybackStatus
        {
            Playing,
            Paused
        }

        private const string ActionPlay = "nz.co.bibble.bibblemp.ACTION_PLAY";
        private const string ActionPause = "nz.co.bibble.bibblemp.ACTION_PAUSE";
        private const string ActionPrevious = "nz.co.bibble.bibblemp.ACTION_PREVIOUS";
        private const string ActionNext = "nz.co.bibble.bibblemp.ACTION_NEXT";
        private const string ActionStop = "nz.co.bibble.bibblemp.ACTION_STOP";

        // Media Session
        private MediaSessionCompat _mediaSession;

        // Media Session Manager
        private MediaSessionManager _mediaSessionManager;

        // Media Controller 
        private MediaControllerCompat.TransportControls _transportControls;

        // Audio List 
        private IList<PlayListTrack> _audioList;

        // Audio List 
        private int _audioIndex;

        // Current Track
        private PlayListTrack _activeAudio;

        // Media player
        private MediaPlayer _player;

        // Audio Manager
        private AudioManager _audioManager;

        // Cached file index
        private int _currentMediaFileCacheFileIndex;

        // Cached Filename
        private readonly MediaFileCache[] _mediaFileCache = new MediaFileCache[2];


        private int _resumePosition;
        public IBinder MusicBinder;
        public override void OnCreate()
        {
            Log.Debug(TAG, "OnCreate");

            // Create the base service
            base.OnCreate();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Log.Debug(TAG, "OnStartCommand");

            try
            {
                //Load data from SharedPreferences
                var myStorage = new PlayListStorage(ApplicationContext);

                _audioList = myStorage.LoadAudio();

                _audioIndex = myStorage.LoadAudioIndex();

                if (_audioIndex != -1 && _audioIndex < _audioList.Count)
                {
                    // Index is in a valid range
                    _activeAudio = _audioList[_audioIndex];
                }
                else
                {
                    StopSelf();
                }
            }
            catch
            {
                StopSelf();
            }

            //Request audio focus
            if (RequestAudioFocus() == false)
            {
                //Could not gain focus
                StopSelf();
            }

            if (_audioManager == null)
            {
                try
                {
                    InitializeMediaSession();
                    InitializeMusicPlayer();
                }
                catch (RemoteException e)
                {
                    e.PrintStackTrace();
                    StopSelf();
                }
                BuildNotification(PlaybackStatus.Playing);
            }

            // Handle Intent action from MediaSession. TransportControls
            HandleIncomingActions(intent);

            return base.OnStartCommand(intent, flags, startId);
        }

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug(TAG, "OnBind");
            MusicBinder = new MusicBinder(this);
            return MusicBinder;
        }
        
        public override bool OnUnbind(Intent intent)
        {
            Log.Debug(TAG, $"OnUnbind.");
            _player.Stop();
            _player.Release();
            return base.OnUnbind(intent);
        }
        
        public override void OnDestroy()
        {
            base.OnDestroy();

            Log.Debug(TAG, $"OnDestroy.");

            if (_player == null) return;

            _player.Release();
            _player = null;
        }

        public void OnPrepared(MediaPlayer mp)
        {
            //    _player.Start();
        }

        public bool OnError(MediaPlayer mp, MediaError what, int extra)
        {
            //Invoked when there has been an error during an asynchronous operation
            switch (what)
            {
                case MediaError.Io:
                    Log.Debug(TAG, $"MediaPlayer Error: IO Error. Extra data {extra}.");
                    break;
                case MediaError.Malformed:
                    Log.Debug(TAG, $"MediaPlayer Error: Malformed. Extra data {extra}.");
                    break;
                case MediaError.NotValidForProgressivePlayback:
                    Log.Debug(TAG, $"MediaPlayer Error: Not Valid for Progressive Playback. Extra data {extra}.");
                    break;
                case MediaError.ServerDied:
                    Log.Debug(TAG, $"MediaPlayer Error: Server Died. Extra data {extra}.");
                    break;
                case MediaError.TimedOut:
                    Log.Debug(TAG, $"MediaPlayer Error: Timed out. Extra data {extra}.");
                    break;
                case MediaError.Unknown:
                    Log.Debug(TAG, $"MediaPlayer Error: Media Error Unknown. Extra data {extra}.");
                    break;
                case MediaError.Unsupported:
                    Log.Debug(TAG, $"MediaPlayer Error: Unsupported. Extra data {extra}.");
                    break;
                // default:
                    // throw new ArgumentOutOfRangeException(nameof(what), what, null);
            }

            return false;
        }

        public void OnCompletion(MediaPlayer mp)
        {
            _currentMediaFileCacheFileIndex++;
            if (_currentMediaFileCacheFileIndex <= (_mediaFileCache.Length - 1))
            {
                _audioIndex = _mediaFileCache[_currentMediaFileCacheFileIndex].AudioIndex;
            }
            else
            {
                _currentMediaFileCacheFileIndex = 0;
                _audioIndex = _mediaFileCache[0].AudioIndex;
            }

            PlaySong(_audioIndex);
        }

        public void InitializeMusicPlayer()
        {
            Log.Debug(TAG, "InitializeMusicPlayer()");

            //initialize position
            _audioIndex = 0;

            // Initialize Media Player
            Log.Debug(TAG, "Creating player");

            _player = new MediaPlayer();

            // set player properties
            _player.SetWakeMode(ApplicationContext, WakeLockFlags.Partial);

            var myAttributes = new AudioAttributes.Builder().SetFlags(AudioFlags.None).SetLegacyStreamType(Stream.Music).Build();
            _player.SetAudioAttributes(myAttributes);

            //set listeners
            _player.SetOnPreparedListener(this);
            _player.SetOnCompletionListener(this);
            _player.SetOnErrorListener(this);

            // Reset so that the MediaPlayer is not pointing to another data source
            _player.Reset();
        }

        //play a song
        public void PlaySong(int position)
        {
            try
            {
                _audioIndex = position;

                if (_mediaFileCache.Length > 0 && _mediaFileCache[0] == null)
                {
                    SetPlayableFileInCache(_currentMediaFileCacheFileIndex,true);
                }

                // Reset player
                _player.Reset();

                // Set Data Source
                Log.Debug(TAG, $"About to Set Data Source to temp file {_mediaFileCache[_currentMediaFileCacheFileIndex].Filename}, Current Cached File Index is {_currentMediaFileCacheFileIndex}.");
                _player.SetDataSource(_mediaFileCache[_currentMediaFileCacheFileIndex].Filename);

                // Prepare Async
                _player.Prepare();
                _player.Start();

                // Set Current Index in Cache
                var myNextMediaFileCacheFileIndex = (_currentMediaFileCacheFileIndex + 1);
                if (myNextMediaFileCacheFileIndex > (_mediaFileCache.Length - 1))
                {
                    myNextMediaFileCacheFileIndex = 0;
                }

                SetPlayableFileInCache(myNextMediaFileCacheFileIndex);
            }
            catch (Exception e)
            {
                Log.Error("MUSIC SERVICE", "Error setting data source", e);
            }
        }

        private void SetPlayableFileInCache(int positionInCache, bool firstFile = false)
        {
            var myFileOk = false;
            var myIndex = _audioIndex;
            if (!firstFile)
            {
                myIndex++;
            }

            do
            {
                try
                {
                    var myTask = SambaSubsystem.Instance.GetSambaFileAsync(_audioList[myIndex].Filename);

                    myTask.Wait();

                    var myNextPlayableFile = myTask.Result.Path;

                    var myMp3Info = Mp3Info.GetMp3Id3V2Info(myNextPlayableFile);

                    if (myMp3Info.Id3Result == Mp3Id3V2Info.Id3ResultTypes.Id3ResultFileOk)
                    {
                        Log.Debug(TAG, $"Caching next file at cache Index {positionInCache} position {myIndex}. {_audioList[myIndex].DisplayName}.");

                        var myMediaFileCache = new MediaFileCache
                        {
                            Filename = myNextPlayableFile,
                            PositionInCache = _currentMediaFileCacheFileIndex,
                            AudioIndex = myIndex,
                            TemportyFile = myTask.Result,
                            ArtImage = myMp3Info.ArtImage
                        };

                        _mediaFileCache[positionInCache] = myMediaFileCache;

                        myFileOk = true;
                    }
                    myIndex++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            } while (myIndex <= (_audioList.Count - 1) && myFileOk == false);
        }

        public void OnPlay()
        {
            _player.Start();
        }

        public void OnStop()
        {
            if (_player == null) return;

            if (_player.IsPlaying)
            {
                _player.Stop();
            }
        }
        public void OnPause()
        {
            if (_player.IsPlaying)
            {
                _player.Pause();
                _resumePosition = _player.CurrentPosition;
            }
        }
        public void OnResume()
        {
            if (_player.IsPlaying) return;

            _player.SeekTo(_resumePosition);
            _player.Start();
        }

        public int GetCurrentPosition()
        {
            return _player.CurrentPosition;
        }
        public void SkipToNext()
        {

            if (_audioIndex == _audioList.Count - 1)
            {
                // If last in playlist
                _audioIndex = 0;
                _activeAudio = _audioList[_audioIndex];
            }
            else
            {
                //get next in playlist
                _activeAudio = _audioList[++_audioIndex];
            }

            //Update stored index
            new PlayListStorage(ApplicationContext).StoreAudioIndex(_audioIndex);

            OnStop();

            //reset mediaPlayer
            _player.Reset();
            InitializeMusicPlayer();
        }

        public void SkipToPrevious()
        {
            if (_audioIndex == 0)
            {
                //if first in playlist
                //set index to the last of audioList
                _audioIndex = _audioList.Count - 1;
                _activeAudio = _audioList[_audioIndex];
            }
            else
            {
                //get previous in playlist
                _activeAudio = _audioList[--_audioIndex];
            }

            //Update stored index
            new PlayListStorage(ApplicationContext).StoreAudioIndex(_audioIndex);

            OnStop();

            // Reset mediaPlayer
            _player.Reset();
            InitializeMusicPlayer();
        }

        public int GetDuration()
        {
            var myDuration = 0;

            if (_player.IsPlaying)
            {
                myDuration = _player.Duration;
            }
            return myDuration;
        }

        public void SeekTo(int currentPosition)
        {
            _player.SeekTo(currentPosition);
        }
        private bool RequestAudioFocus()
        {

            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                _audioManager = (AudioManager)GetSystemService(AudioService);

                var myAudioAttributes = new AudioAttributes.Builder().SetLegacyStreamType(Stream.Music).Build();

                var myAudioFocusRequestClass = new AudioFocusRequestClass.Builder(AudioFocus.Gain)
                    .SetAudioAttributes(myAudioAttributes)
                    .SetOnAudioFocusChangeListener(this)
                    .Build();

                var myAudioFocusRequest = _audioManager.RequestAudioFocus(myAudioFocusRequestClass);

                return myAudioFocusRequest == AudioFocusRequest.Granted;
            }
            else
            {
#pragma warning disable 618
                var myAudioFocusRequest = _audioManager?.RequestAudioFocus(
                    this, 
                    Stream.Music, 
                    AudioFocus.Gain);
#pragma warning restore 618
                return myAudioFocusRequest == AudioFocusRequest.Granted;
            }
        }

        public PendingIntent PlaybackAction(int actionNumber)
        {
            var playbackAction = new Intent(this, typeof(MusicService));

            switch (actionNumber)
            {
                case 0:
                    // Play
                    playbackAction.SetAction(ActionPlay);
                    return PendingIntent.GetService(this, actionNumber, playbackAction, 0);
                case 1:
                    // Pause
                    playbackAction.SetAction(ActionPause);
                    return PendingIntent.GetService(this, actionNumber, playbackAction, 0);
                case 2:
                    // Next track
                    playbackAction.SetAction(ActionNext);
                    return PendingIntent.GetService(this, actionNumber, playbackAction, 0);
                case 3:
                    // Previous track
                    playbackAction.SetAction(ActionPrevious);
                    return PendingIntent.GetService(this, actionNumber, playbackAction, 0);
            }
            return null;
        }

        private void InitializeMediaSession()
        {
            if (_mediaSessionManager != null) return; //mediaSessionManager exists

            _mediaSessionManager = (MediaSessionManager)GetSystemService(MediaSessionService);

            // Create a new MediaSession
            _mediaSession = new MediaSessionCompat(ApplicationContext, "AudioPlayer");

            //Get MediaSessions transport controls
            _transportControls = _mediaSession.Controller.GetTransportControls();

            //set MediaSession -> ready to receive media commands
            _mediaSession.Active = true;

            // Indicate that the MediaSession handles transport control commands
            // through its MediaSessionCompat.Callback.
            _mediaSession.SetFlags(MediaSessionCompat.FlagHandlesTransportControls);

            //Set mediaSession's MetaData
            UpdateMetaData();

            _mediaSession.SetCallback(new MediaSessionCallback(this));
        }

        public void UpdateMetaData()
        {
            if (_activeAudio == null) return;

            //replace with medias albumArt
            var albumArt = BitmapFactory.DecodeResource(Resources, Resource.Drawable.logo);

            _mediaSession.SetMetadata(new MediaMetadataCompat.Builder()
                .PutBitmap(MediaMetadataCompat.MetadataKeyAlbumArt, albumArt)
                .PutString(MediaMetadataCompat.MetadataKeyArtist, _activeAudio.Artist)
                .PutString(MediaMetadataCompat.MetadataKeyAlbum, _activeAudio.Album)
                .PutString(MediaMetadataCompat.MetadataKeyTitle, _activeAudio.Title)
                .Build());
        }
        
        public void BuildNotification(PlaybackStatus playbackStatus)
        {
            var myNotificationAction = Android.Resource.Drawable.IcMediaPause; //needs to be initialized

            PendingIntent myPlayPausePendingIntent = null;

            //Build a new notification according to the current state of the MediaPlayer
            if (playbackStatus == PlaybackStatus.Playing)
            {
                myNotificationAction = Android.Resource.Drawable.IcMediaPause;

                //create the pause action
                myPlayPausePendingIntent = PlaybackAction(1);
            }
            else if (playbackStatus == PlaybackStatus.Paused)
            {
                myNotificationAction = Android.Resource.Drawable.IcMediaPlay;

                //create the play action
                myPlayPausePendingIntent = PlaybackAction(0);
            }

            var myLargeIcon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.logo); //replace with your own image

            var myStyle = new Android.Support.V4.Media.App.NotificationCompat.MediaStyle();
            myStyle.SetMediaSession(_mediaSession.SessionToken);
            myStyle.SetShowActionsInCompactView(0, 1, 2);

            var mySmallIcon = Android.Resource.Drawable.StatSysHeadset;

            var myColor = Color.ParseColor("#2c3e50");
            // var myColor = ContextCompat.GetColor(this, Color.ParseColor("#2c3e50"));

            // Find the Cached 
            if (_mediaFileCache?[_currentMediaFileCacheFileIndex]?.ArtImage != null)
            {
                var myArtImage = _mediaFileCache[_currentMediaFileCacheFileIndex].ArtImage;
            }

            // Create a new Notification
            var myNotificationBuilder = new NotificationCompat.Builder(this)

                // Set the Notification style
                .SetStyle(myStyle)

                .SetShowWhen(false)

                // Set the Notification color
                .SetColor(myColor)

                // Set the large and small icons
                .SetLargeIcon(myLargeIcon)
                .SetSmallIcon(mySmallIcon)

                // Set Notification content information
                .SetContentText(_activeAudio.Artist)
                .SetContentTitle(_activeAudio.Album)
                .SetContentInfo(_activeAudio.Title)

                // Add playback actions
                .AddAction(Android.Resource.Drawable.IcMediaPrevious, "previous", PlaybackAction(3))
                .AddAction(myNotificationAction, "pause", myPlayPausePendingIntent)
                .AddAction(Android.Resource.Drawable.IcMediaNext, "next", PlaybackAction(2));
        
            ((NotificationManager)GetSystemService(NotificationService)).Notify(NotificationId, myNotificationBuilder.Build());
        }

        public void RemoveNotification()
        {
            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Cancel(NotificationId);
        }
        private void HandleIncomingActions(Intent playbackAction)
        {
            if (playbackAction?.Action == null) return;

            var actionString = playbackAction.Action;

            if (actionString.Equals(ActionPlay, StringComparison.InvariantCultureIgnoreCase))
            {
                _transportControls.Play();
            }
            else if (actionString.Equals(ActionPause, StringComparison.InvariantCultureIgnoreCase))
            {
                _transportControls.Pause();
            }
            else if (actionString.Equals(ActionNext, StringComparison.InvariantCultureIgnoreCase))
            {
                _transportControls.SkipToNext();
            }
            else if (actionString.Equals(ActionPrevious, StringComparison.InvariantCultureIgnoreCase))
            {
                _transportControls.SkipToPrevious();
            }
            else if (actionString.Equals(ActionStop,StringComparison.InvariantCultureIgnoreCase))
            {
                _transportControls.Stop();
            }
        }

        public void OnAudioFocusChange(AudioFocus focusChange)
        {
            //Invoked when the audio focus of the system is updated.
            switch (focusChange)
            {
                case AudioFocus.Gain:

                    // resume playback
                    if (_player == null)
                    {
                        InitializeMusicPlayer();
                    }
                    else if (!_player.IsPlaying)
                    {
                        _player.Start();
                    }
                    _player.SetVolume(1.0f, 1.0f);
                    break;

                case AudioFocus.GainTransient:
                    break;
                case AudioFocus.GainTransientExclusive:
                    break;
                case AudioFocus.GainTransientMayDuck:
                    break;
                case AudioFocus.Loss:

                    // Lost focus for an unbounded amount of time: stop playback and release media player
                    if (_player.IsPlaying) _player.Stop();

                    _player.Release();
                    _player = null;

                    break;
                case AudioFocus.LossTransient:

                    // Lost focus for a short time, but we have to stop
                    // playback. We don't release the media player because playback
                    // is likely to resume
                    if (_player.IsPlaying) _player.Pause();
                    break;

                case AudioFocus.LossTransientCanDuck:

                    // Lost focus for a short time, but it's ok to keep playing
                    // at an attenuated level
                    if (_player.IsPlaying) _player.SetVolume(0.1f, 0.1f);
                    break;

                case AudioFocus.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(focusChange), focusChange, null);
            }
        }

        public void PlayNewSong(int position)
        {
            _mediaFileCache[0] = null;
            PlaySong(position);
        }
    }
}
