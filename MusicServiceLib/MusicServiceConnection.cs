﻿using Android.Content;
using Android.OS;
using Android.Util;
using MusicServiceLib;

namespace bibbleMP
{
    public class MusicServiceConnection : Java.Lang.Object, IServiceConnection
    {
        static readonly string TAG = typeof(MusicServiceConnection).FullName;

        public bool IsConnected { get; private set; }
        public MusicBinder Binder { get; private set; }
        
        public MusicServiceConnection()
        {
            IsConnected = false;
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            Log.Debug(TAG, $"OnServiceConnected {name.ClassName}");

            Binder = service as MusicBinder;
            IsConnected = Binder != null;

            var message = "onServiceConnected - ";
            if (IsConnected)
            {
                message = message + " bound to service " + name.ClassName;
                // mainActivity.UpdateUiForBoundService();
            }
            else
            {
                message = message + " not bound to service " + name.ClassName;
                // mainActivity.UpdateUiForUnboundService();
            }

            Log.Info(TAG, message);
            // mainActivity.timestampMessageTextView.Text = message;

        }

        public void OnServiceDisconnected(ComponentName name)
        {
            Log.Debug(TAG, $"OnServiceDisconnected {name.ClassName}");
            IsConnected = false;
            Binder = null;
            //mainActivity.UpdateUiForUnboundService();
        }
        /*
        public int GetDuration()
        {
            var myDuration = 0;
            if (IsConnected && Binder != null)
            {
                myDuration = Binder.Service.GetDuration();
            }

            return myDuration;
        }
        public long GetCurrentPosition()
        {
            var myDuration = 0L;
            if (IsConnected && Binder != null)
            {
                myDuration = Binder.Service.GetCurrentPosition();
            }

            return myDuration;
        }

        public void PlaySong(int position)
        {
            if (IsConnected)
            {
                Binder?.Service.PlaySong(position);
            }
        }

        public void StopPlayer()
        {
            if (IsConnected)
            {
                Binder?.Service.OnStop();
            }
        }

        public void StartPlayer()
        {
            if (IsConnected)
            {
                Binder?.Service.OnPlay();
            }
        }

        public void PausePlayer()
        {
            if (IsConnected)
            {
                Binder?.Service.OnPause();
            }
        }

        public void SeekTo(int currentPosition)
        {
            if (IsConnected)
            {
                Binder?.Service.SeekTo(currentPosition);
            }
        }
        */
    }
}