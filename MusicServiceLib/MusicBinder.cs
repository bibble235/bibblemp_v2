﻿using Android.OS;

namespace MusicServiceLib
{
    public class MusicBinder : Binder
    {
        public MusicBinder(MusicService service)
        {
            Service = service;
        }
        public MusicService Service {get; }
    }
}