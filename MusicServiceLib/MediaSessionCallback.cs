﻿using Android.OS;
using Android.Support.V4.Media.Session;

namespace MusicServiceLib
{
    public class MediaSessionCallback :  MediaSessionCompat.Callback 
    {
        private MusicService _musicService;

        public MediaSessionCallback(MusicService musicService)
        {
            _musicService = musicService;
        }

        public override void OnPlay()
        {
            base.OnPlay();
            _musicService.OnResume();
            _musicService.BuildNotification(MusicService.PlaybackStatus.Playing);
        }
        public override void OnPause()
        {
            base.OnPause();
            _musicService.OnPause();
            _musicService.BuildNotification(MusicService.PlaybackStatus.Paused);
        }

        public override void OnSkipToNext()
        {
            base.OnSkipToNext();
            _musicService.SkipToNext();
            _musicService.UpdateMetaData();
            _musicService.BuildNotification(MusicService.PlaybackStatus.Playing);
        }

        public override void OnSkipToPrevious()
        {
            base.OnSkipToPrevious();
            _musicService.SkipToPrevious();
            _musicService.UpdateMetaData();
            _musicService.BuildNotification(MusicService.PlaybackStatus.Playing);
        }

        public override void OnStop()
        {
            base.OnStop();
            _musicService.RemoveNotification();
            _musicService.StopSelf();
        }
    }
}
