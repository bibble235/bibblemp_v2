﻿using System.IO;
using AndroidCommonLib;

namespace MusicServiceLib
{
    public class MediaFileCache
    {
        public int PositionInCache { get; set; }
        public int AudioIndex { get; set; }
        public string Filename { get; set; }
        public TemporaryFile TemportyFile { get; set; }
        public MemoryStream ArtImage { get; set; }
    }
}