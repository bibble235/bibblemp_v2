﻿using System;
using System.IO;
using System.Threading.Tasks;
using AndroidCommonLib;
using SharpCifs.Smb;
using SharpCifs.Util.Sharpen;
using Console = System.Console;
using InputStream = SharpCifs.Util.Sharpen.InputStream;

namespace SambaLib
{
    public class SambaSubsystem
    {
        private static SambaSubsystem _instanceSambaSubsystem;

        public static SambaSubsystem Instance => _instanceSambaSubsystem ?? (_instanceSambaSubsystem = new SambaSubsystem());

        public bool TestConnection(string ip, string shareName, string username, string password)
        {
            var myResult = false;

            try
            {
                var myAuthentication = new NtlmPasswordAuthentication(null, username, password);
                
                var mySmbFile = new SmbFile($"smb://{ip}/{shareName}/", myAuthentication);

                // ReSharper disable once UnusedVariable
                var myRoot = Arrays.AsList(mySmbFile.ListFiles());
                if (mySmbFile.IsDirectory())
                {
                    myResult = true;
                }
            }
            catch (Exception myException)
            {
                Console.WriteLine(myException);
            }
            return myResult;
        }

        public async Task<TemporaryFile> GetSambaFileAsync(string filename)
        {
            InputStream myInputStream = null;
            var myTemporaryFile = new TemporaryFile();

            try
            {
                var mySambaFileObject = new SambaFileObject(filename);

                var mySmbFile = new SmbFile(mySambaFileObject.SambaFilename, mySambaFileObject.Authentication);
                myInputStream = mySmbFile.GetInputStream();

                using (var myOutputStream = new FileStream(myTemporaryFile.Path, FileMode.Create))
                {
                    ((Stream)myInputStream).CopyTo(myOutputStream);
                }
                myInputStream?.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                myInputStream?.Close();
            }

            return await Task.FromResult(myTemporaryFile);
        }

        public Stream GetNBytesOfSambaFile(string filename, long numberOfBytes)
        {
            var mySambaFileObject = new SambaFileObject(filename);
            return GetNBytesOfSambaFile(mySambaFileObject, numberOfBytes);
        }

        public Stream GetNBytesOfSambaFile(SambaFileObject sambaFileObject, long numberOfBytes)
        {
            byte[] myBuffer = new byte[numberOfBytes];

            InputStream myInputStream = null;
            
            try
            {
                var mySmbFile = new SmbFile(sambaFileObject.SambaFilename, sambaFileObject.Authentication);
                myInputStream = mySmbFile.GetInputStream();
                myInputStream.Read(myBuffer, 0, myBuffer.Length);
                myInputStream?.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                myInputStream?.Close();
            }

            return new MemoryStream(myBuffer);
        }

        public bool CheckFileExists(string filename)
        {
            var myResult = false;
            var mySambaFileObject = new SambaFileObject(filename);
            using (var mySambaStreamFileReader = new SambaFileReader(mySambaFileObject))
            {
                myResult = mySambaStreamFileReader.OpenSambaFile();
            }
            return myResult;
        }
    }
}
