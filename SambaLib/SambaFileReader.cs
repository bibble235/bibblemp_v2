﻿using System;
using System.IO;
using AndroidCommonLib;
using SharpCifs.Smb;
using Console = System.Console;
using InputStream = SharpCifs.Util.Sharpen.InputStream;

namespace SambaLib
{
    public class SambaFileReader : ICommonStreamReader
    {
        private long _totalSambaBytesRead;
        private readonly SambaFileObject _sambaFileObject;
        private InputStream _inputStream;

        public MemoryStream MemoryStream { get; }

        public SambaFileReader(SambaFileObject sambaFileObject)
        {
            _sambaFileObject = sambaFileObject;
            MemoryStream = new MemoryStream();
        }

        public bool OpenSambaFile()
        {
            bool myFileExists;

            try
            {
                var mySmbFile = new SmbFile(_sambaFileObject.SambaFilename, _sambaFileObject.Authentication);
                myFileExists = mySmbFile.Exists();
                if (myFileExists)
                {
                    _inputStream = mySmbFile.GetInputStream();
                }
            }
            catch
            {
                myFileExists = false;
                Console.WriteLine("All gone wrong");
            }

            return myFileExists;
        }

        public long SambaFileLength => _inputStream.Length;

        public bool CanRead => true;

        public long Seek(int offset, SeekOrigin seekOrigin)
        {
            long myLength = -1;
            if (seekOrigin == SeekOrigin.Begin)
            {
                if (MemoryStream.Length < offset - 1)
                {
                    var myNumberOfSambaBytesToRead = offset - MemoryStream.Length;
                    ReadSambaBytesToMemoryStream(Convert.ToInt32(myNumberOfSambaBytesToRead));
                }
                myLength = MemoryStream.Seek(offset, SeekOrigin.Begin);
            }
            return myLength;
        }


        public int Read(byte[] bytes, int offset,int numberOfBytes)
        {
            // If we have done a seek some of the bytes might have already 
            // been read
            var myAlreadyReadSambaBytes = MemoryStream.Length - MemoryStream.Position;

            // Calculate Number of Bytes to read 
            var myNumberOfSambaBytesToRead = numberOfBytes - myAlreadyReadSambaBytes;

            if (myNumberOfSambaBytesToRead > 0)
            {
                ReadSambaBytesToMemoryStream(Convert.ToInt32(myNumberOfSambaBytesToRead));
            }
            return MemoryStream.Read(bytes,offset, numberOfBytes);
        }

        public long Length => _inputStream.Length;
        public long Position => MemoryStream.Position;

        /*
         * Read Bytes from the Samba Input Stream and copy them to the memory stream
         */
        private void ReadSambaBytesToMemoryStream(int numberOfBytes)
        {
            var myBuffer = new byte[numberOfBytes];

            var mySambaBytesRead =_inputStream.Read(myBuffer, 0, numberOfBytes);
            _totalSambaBytesRead = _totalSambaBytesRead + mySambaBytesRead;

            // Always copy samba bytes to the end
            //            _memoryStream.Seek(_memoryStream.Length, SeekOrigin.Begin);
            var myCurrentPosition = MemoryStream.Position;

            MemoryStream.Seek(0, SeekOrigin.End);
            MemoryStream.Write(
                myBuffer,
                0, 
                mySambaBytesRead);

            MemoryStream.Position = myCurrentPosition;
        }

        public void Dispose()
        {
            _inputStream?.Close();
            _inputStream?.Dispose();
        }
    }
}