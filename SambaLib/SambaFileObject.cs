﻿using ConfigurationLib;
using SharpCifs.Smb;

namespace SambaLib
{
    public class SambaFileObject
    {
        public string Filename { get; }
        public string SambaFilename {get;}
        public NtlmPasswordAuthentication Authentication {get;}

        public SambaFileObject(string filename)
        {
            Filename = filename;

            SambaConnection configurationSamba = ConfigurationSubsystem.Instance.GetSambaConnectionForFilename(filename);
            var username = configurationSamba.Username;
            var password = configurationSamba.Password;
            var ip = configurationSamba.IP;
            Authentication = new NtlmPasswordAuthentication(null, username, password);

            var myFilenameTemp = filename.ToUpper().Replace('\\', '/');
            var myIndex = GetNthIndex(myFilenameTemp, '/', 3);
            myFilenameTemp = myFilenameTemp.Substring(myIndex);

            SambaFilename = $"smb://{ip}{myFilenameTemp}";
        }

        // ReSharper disable once InconsistentNaming
        private static int GetNthIndex(string s, char t, int n)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == t)
                {
                    count++;
                    if (count == n)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

    }
}