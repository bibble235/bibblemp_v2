﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using ConfigurationLib;
using Newtonsoft.Json;
using Exception = Java.Lang.Exception;

namespace ConfigurationLib
{
    public class ConfigurationSubsystem
    {
        private Context context;

        private const string CONNECTIONS = "Connections";
        private const string PLAYLISTS = "Playlists";

        public IDictionary<string, string> StringConfigurations = new Dictionary<string, string>();

        public static ConfigurationSubsystem Instance { get; set; }

        public ConfigurationSubsystem(Context context)
        {
            Instance = this;
            this.context = context;

            // DeleteConfigurations(CONNECTIONS);
        }

        public void AddSambaConnection(SambaConnection sambaConnection)
        {
            var mySambaConnections = GetSambaConnections();
            mySambaConnections.Add(sambaConnection);
            SetSambaConnections(mySambaConnections);
        }

        public SambaConnection GetSambaConnectionForId(string Id)
        {
            var mySambaConnections = GetSambaConnections();
            return mySambaConnections.FirstOrDefault(x => x.Id == Id);
        }

        public void DeleteSambaConnectionForId(string Id)
        {
            var mySambaConnections = GetSambaConnections();
            var mySambaConnection = mySambaConnections.Where(x => x.Id != Id).ToList();
            SetSambaConnections(mySambaConnections);
        }

        public void UpdateSambaConnectionForId(SambaConnection sambaConnection)
        {
            var mySambaConnections = GetSambaConnections();
            var myIndex = mySambaConnections.FindIndex(
                    x => x.Id == sambaConnection.Id);
            mySambaConnections[myIndex] = sambaConnection;
            SetSambaConnections(mySambaConnections);
        }

        public void AddConfigurationPlaylists(ConfigurePlaylist playlist)
        {
            var myConfigurationPlaylists = GetConfigurationPlaylists();
            myConfigurationPlaylists.Add(playlist);
            SetPlaylistConfigurations(myConfigurationPlaylists);
        }

        public ConfigurePlaylist GetConfigurationPlaylistForId(string Id)
        {
            var myConfigurationPlaylist = GetConfigurationPlaylists();
            return myConfigurationPlaylist.FirstOrDefault(x => x.Id == Id);
        }

        public void DeleteConfigurationPlaylistConnectionForId(string Id)
        {
            var myConfigurationPlaylists = GetConfigurationPlaylists();
            myConfigurationPlaylists = myConfigurationPlaylists.Where(x => x.Id != Id).ToList();
            SetPlaylistConfigurations(myConfigurationPlaylists);
        }

        public void UpdateConfigurationPlaylistForId(ConfigurePlaylist configurationPlaylist)
        {
            var myConfigurationPlaylists = GetConfigurationPlaylists();
            var myIndex = myConfigurationPlaylists.FindIndex(
                x => x.Id == configurationPlaylist.Id);
            myConfigurationPlaylists[myIndex] = configurationPlaylist;
            SetPlaylistConfigurations(myConfigurationPlaylists);
        }

        public List<SambaConnection> GetSambaConnections()
        {
            List<SambaConnection> sambaConnections = null;

            var myValue = GetConfigurations(CONNECTIONS);

            if (!string.IsNullOrEmpty(myValue))
            {
                sambaConnections = JsonConvert.DeserializeObject<List<SambaConnection>>(myValue);
            }
            else
            {
                sambaConnections = new List<SambaConnection>();
            }
            return sambaConnections;
        }

        public List<ConfigurePlaylist> GetConfigurationPlaylists()
        {
            List<ConfigurePlaylist> myConfigurationPlaylists = null;

            var myValue = GetConfigurations(PLAYLISTS);

            if (!string.IsNullOrEmpty(myValue))
            {
                myConfigurationPlaylists = JsonConvert.DeserializeObject<List<ConfigurePlaylist>>(myValue);
            }
            else
            {
                myConfigurationPlaylists = new List<ConfigurePlaylist>();
            }
            return myConfigurationPlaylists;
        }

        private void SetSambaConnections(List<SambaConnection> sambaConnections)
        {
            var myValue = JsonConvert.SerializeObject(sambaConnections);
            SetConfigurations(CONNECTIONS, myValue);
        }

        private void SetPlaylistConfigurations(IList<ConfigurePlaylist> playlists)
        {
            var myValue = JsonConvert.SerializeObject(playlists);
            SetConfigurations(PLAYLISTS,myValue);
        }

        private void DeleteConfigurations(string key)
        {
            ISharedPreferences nySharedPreferences = context.GetSharedPreferences(key, FileCreationMode.Private);
            ISharedPreferencesEditor editor = nySharedPreferences.Edit();
            editor.Remove(key);
            editor.Apply(); ;
        }

        private void SetConfigurations(string key, string value)
        {
            ISharedPreferences nySharedPreferences = context.GetSharedPreferences(key, FileCreationMode.Private);
            ISharedPreferencesEditor editor = nySharedPreferences.Edit();
            editor.PutString(key, value);
            editor.Apply(); ;
        }

        private string GetConfigurations(string key)
        {
            var myPreferences = context.GetSharedPreferences(key, FileCreationMode.Private);

            var myValue = myPreferences.GetString(key, "");

            return myValue;
        }

        public SambaConnection GetSambaConnectionForFilename(string filename)
        {
            var myFilenameParts = filename.Split('\\');

            if(myFilenameParts.Length < 3)
                throw new Exception($"filename {filename} is not in the correct format");

            var mySambaConfigurations = GetSambaConnections();
            var mySambaConnection = mySambaConfigurations.FirstOrDefault(
                x => 
                    string.Equals(x.Hostname, myFilenameParts[2], StringComparison.CurrentCultureIgnoreCase) && 
                    string.Equals(x.Sharename, myFilenameParts[3], StringComparison.CurrentCultureIgnoreCase));

            if (mySambaConnection == null)
                throw new Exception($"Either host or sharename is not configure for filename {filename}.");

            return mySambaConnection;
        }
    }
}
