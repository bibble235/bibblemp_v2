﻿namespace ConfigurationLib
{
    public class ConfigurePlaylist
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}