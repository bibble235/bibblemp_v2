﻿namespace ConfigurationLib
{
    public class SambaConnection
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public string Hostname { get; set; }
        public string Sharename { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
