﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DataLib;
using Java.IO;
using PlaylistsNET.Content;
using PlaylistsNET.Models;
using Console = System.Console;

namespace WindowsMediaPlayerLib
{
    public class WindowsMediaPlayerSubsystem
    {
        private static WindowsMediaPlayerSubsystem _instanceSambaSubsystem;
        public static WindowsMediaPlayerSubsystem Instance => _instanceSambaSubsystem ?? (_instanceSambaSubsystem = new WindowsMediaPlayerSubsystem());

        public async Task<IList<PlayListTrack>> GetPlayListTracks(string playlistFilename)
        {
            var myPlayList = new List<PlayListTrack>();
            
            using (var myInputStream = new FileStream(playlistFilename, FileMode.Open))
            {
                var content = new WplContent();
                var playlist = content.GetFromStream(myInputStream);

                foreach (var myItem in playlist.PlaylistEntries)
                {
                    myPlayList.Add(new PlayListTrack(myItem.Path));
                }
            }
            return await Task.FromResult(myPlayList);
        }
    }
}
