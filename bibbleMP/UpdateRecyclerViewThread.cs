﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Android.OS;
using Android.Support.V7.Widget;
using DataLib;
using Mp3InfoLib;
using SambaLib;

namespace bibbleMP
{
    public class UpdateRecyclerViewThread : AsyncTask<string,int,bool>
    {
        private readonly IDictionary<int, PlayListTrack> _listOfPositionsToUpdate = new Dictionary<int, PlayListTrack>();
        public RecyclerView PlayListRecyclerView;
        public PlayListAdapter PlayListAdapter;
        public bool StopThread = false;

        private readonly object _lockItem = new object();
        public void OnAddUpdate(int position, PlayListTrack playListTrack)
        {
            lock (_lockItem)
            {
                _listOfPositionsToUpdate[position] = playListTrack;
            }
        }
        private void OnUpdateTrackLength(int position)
        {
            try
            {
                // DoTest(_listOfPositionsToUpdate[position].Filename);

                // Console.WriteLine($"Currently processing {_listOfPositionsToUpdate[position].DisplayName}");

                if(_listOfPositionsToUpdate[position].DisplayName == "03  Fairground Attraction - The First Of A Million Kisses - Moon On The Rain.flac")
                {
                    Console.WriteLine("Custard");
                }

                var myTrack = _listOfPositionsToUpdate[position];

                var myResult = SambaSubsystem.Instance.CheckFileExists(_listOfPositionsToUpdate[position].Filename);
                
                if (myResult)
                {
                    var myMp3Info = SambaMp3Info.GetMp3Id3V2Info(_listOfPositionsToUpdate[position].Filename);

                    if (myMp3Info.Id3Result == Mp3Id3V2Info.Id3ResultTypes.Id3ResultFileOk)
                    {
                        if (myMp3Info.TrackLength != null)
                        {
                            myTrack.DisplayLength = myMp3Info.TrackLength.DisplayLength;
                        }

                        if (myMp3Info != null)
                        {
                            myTrack.DisplayName = myMp3Info.TrackName;
                        }

                        myTrack.FileIssues = PlayListTrack.FileIssuesReasons.FileOk;
                    }
                    else if (myMp3Info.Id3Result == Mp3Id3V2Info.Id3ResultTypes.Id3ResultFlac)
                    {
                        Console.Write($"Found FLAC file {_listOfPositionsToUpdate[position].DisplayName}");
                        myTrack.FileIssues = PlayListTrack.FileIssuesReasons.FileFormatUnsupported;
                    }
                    else if (myMp3Info.Id3Result == Mp3Id3V2Info.Id3ResultTypes.Id3ResultMpegHeaderNotValid)
                    {
                        Console.Write($"MPEG Header not valid {_listOfPositionsToUpdate[position].DisplayName}");
                        myTrack.FileIssues = PlayListTrack.FileIssuesReasons.FileFormatCorrupt;
                    }
                    else if (myMp3Info.Id3Result == Mp3Id3V2Info.Id3ResultTypes.Id3ResultMpegHeaderNotFound)
                    {
                        Console.Write($"MPEG Header not found {_listOfPositionsToUpdate[position].DisplayName}");
                        myTrack.FileIssues = PlayListTrack.FileIssuesReasons.FileFormatCorrupt;
                    }

                }
                else
                {
                    myTrack.FileIssues = PlayListTrack.FileIssuesReasons.FileNotFound;
                }

                PlayListRecyclerView.Post(new RunnableAnonymousInnerClassHelper(PlayListAdapter, position));
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error Processing {_listOfPositionsToUpdate[position].DisplayName}. Exception was {e}");
            }

            lock (_lockItem)
            {
                _listOfPositionsToUpdate.Remove(position);
            }
        }

        public void ClearUpdates()
        {
            lock (_lockItem)
            {
                _listOfPositionsToUpdate.Clear();
            }
        }
        protected override bool RunInBackground(params string[] @params)
        {
            while (!StopThread)
            {
                Thread.Sleep(500);
                lock (_lockItem)
                {
                    if (_listOfPositionsToUpdate.Count > 0)
                    {
                        foreach (var myPlayListTrack in _listOfPositionsToUpdate.ToList())
                        {
                            OnUpdateTrackLength(myPlayListTrack.Key);
                        }
                    }
                }
            }

            return true;
        }

        private class RunnableAnonymousInnerClassHelper : Java.Lang.Object, Java.Lang.IRunnable
        {
            private PlayListAdapter playListAdapter;
            private int position;
            public RunnableAnonymousInnerClassHelper(PlayListAdapter playListAdapter, int position)
            {
                this.playListAdapter = playListAdapter;
                this.position = position;
            }

            public void Run()
            {
                playListAdapter.NotifyItemChanged(position);
            }
        }
    }
}