﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using DataLib;
using Exception = System.Exception;

namespace bibbleMP
{
    public class PlayListAdapter : RecyclerView.Adapter
    {
        public IList<PlayListTrack> playListTracks = new List<PlayListTrack>();

        public UpdateRecyclerViewThread MyThread;

        private IItemClickListener ItemClickListener;

        public PlayListAdapter(IList<PlayListTrack> playListTracks)
        {
            Refresh(playListTracks);
        }
        public void SetClickListener(IItemClickListener itemClickListener)
        {   
            ItemClickListener = itemClickListener;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var myViewHolder = holder as PlayListViewHolder;

            myViewHolder.DisplayNameTextView.Text = playListTracks[position].DisplayName;
            myViewHolder.DisplayLengthTextView.Text = playListTracks[position].DisplayLength;

            if (playListTracks[position].FileIssues == PlayListTrack.FileIssuesReasons.FileUnknown)
            {
                myViewHolder.ItemView.SetBackgroundColor(Color.Red);
            }
            else if (playListTracks[position].FileIssues == PlayListTrack.FileIssuesReasons.FileOk)
            {
                myViewHolder.ItemView.SetBackgroundColor(Color.Green);
            }
            else if (playListTracks[position].FileIssues == PlayListTrack.FileIssuesReasons.FileNotFound)
            {
                myViewHolder.ItemView.SetBackgroundColor(Color.Yellow);
            }
            else if (playListTracks[position].FileIssues == PlayListTrack.FileIssuesReasons.FileFormatUnsupported)
            {
                myViewHolder.ItemView.SetBackgroundColor(Color.LightBlue);
            }
            else if (playListTracks[position].FileIssues == PlayListTrack.FileIssuesReasons.FileFormatCorrupt)
            {
                myViewHolder.ItemView.SetBackgroundColor(Color.LightGray);
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            PlayListViewHolder myViewHolder;
            var myItemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.playlist_track_row_item, parent, false);
            myViewHolder = new PlayListViewHolder(myItemView,ItemClickListener);
            return myViewHolder;
        }

        public override int ItemCount => playListTracks.Count;

        public void Refresh(IList<PlayListTrack> playListTracks)
        {
            this.playListTracks.Clear();
            // this.playListTracks = playListTracks.OrderBy(x => x.Filename).ToList();
            this.playListTracks = playListTracks;
            NotifyDataSetChanged();
        }

        public void UpdateTrackLengths()
        {
            Console.WriteLine($"XXXXXXXXXXXXXXXXXXXXXX Updating all positions");
            for (var myPosition=0; myPosition < playListTracks.Count; myPosition++)
            {
                MyThread.OnAddUpdate(myPosition, playListTracks[myPosition]);
            }

        }
        private async Task UpdateTrackLength(int position)
        {
            try
            {
                Console.WriteLine($"XXXXXXXXXXXXXXXXXXXXXX Updating position {position}");
                playListTracks[position].DisplayLength = $"66.{position}";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            await Task.Delay(50);
        }

    }
}