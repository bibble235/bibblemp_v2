﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using ConfigurationLib;

namespace bibbleMP
{
    public class ConfigureSambaAdapter : BaseAdapter <SambaConnection>
    {
        public List<SambaConnection>  sambaConnections;
        private Context context;
        public ConfigureSambaAdapter(Context context, List<SambaConnection> sambaConnections)
        {
            this.context = context;
            Refresh(sambaConnections);
        }

        public void Refresh(List<SambaConnection> sambaConnections)
        {
            this.sambaConnections = sambaConnections.OrderBy(x=>x.Name).ToList();
            NotifyDataSetChanged();
        }
        public override SambaConnection this[int position] => sambaConnections[position];

        public override int Count
        {
            get
            {
                return sambaConnections.Count;
            }
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            try
            {
                if (row == null)
                {
                    row = LayoutInflater.From(context).Inflate(Resource.Layout.configure_samba_row_item, null, false);
                }

                TextView txtName = row.FindViewById<TextView>(Resource.Id.name);
                txtName.Text = sambaConnections[position].Name;
                TextView txtIp = row.FindViewById<TextView>(Resource.Id.ip);
                txtIp.Text = sambaConnections[position].IP;
                TextView txtShareName = row.FindViewById<TextView>(Resource.Id.share_name);
                txtShareName.Text = sambaConnections[position].Sharename;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {

            }
            return row;
        }
    }
}