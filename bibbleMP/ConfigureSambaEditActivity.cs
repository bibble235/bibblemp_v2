﻿using System;
using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using ConfigurationLib;

namespace bibbleMP
{
    [Activity(Label = "Edit Samba Configuration", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class ConfigureSambaEditActivity : AppCompatActivity
    {
        private Switch requiresAuthentication;
        private TextView username_label;
        private TextView password_label;
        private EditText name;
        private EditText ip;
        private EditText hostname;
        private EditText sharename;
        private EditText username;
        private TextInputLayout password;

        private string id;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


            SetContentView(Resource.Layout.activity_configure_samba_edit);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar_configure_samba_edit);
            SetSupportActionBar(toolbar);

            username_label = FindViewById<TextView>(Resource.Id.username_label);
            password_label = FindViewById<TextView>(Resource.Id.password_label);

            name = FindViewById<EditText>(Resource.Id.name);
            ip = FindViewById<EditText>(Resource.Id.ip);
            hostname = FindViewById<EditText>(Resource.Id.hostname);
            sharename = FindViewById<EditText>(Resource.Id.share_name);
            username = FindViewById<EditText>(Resource.Id.username);
            password = FindViewById<TextInputLayout>(Resource.Id.password);
            requiresAuthentication = FindViewById<Switch>(Resource.Id.requires_authentication);

            
            var myExtras = Intent.Extras;
            id = myExtras.GetString("id");

            SetContent();

            requiresAuthentication.Click += SetAuthenticationControls;
            SetAuthenticationControls();
        }

        private void SetContent()
        {
            var myConfigurationSamba = ConfigurationSubsystem.Instance.GetSambaConnectionForId(id);
            if (myConfigurationSamba != null)
            {
                name.Text = myConfigurationSamba.Name;
                ip.Text = myConfigurationSamba.IP;
                hostname.Text = myConfigurationSamba.Hostname;
                sharename.Text = myConfigurationSamba.Sharename;
                username.Text = myConfigurationSamba.Username;
                password.EditText.Text = myConfigurationSamba.Password;
                if (!string.IsNullOrEmpty(myConfigurationSamba.Username) || !string.IsNullOrEmpty(myConfigurationSamba.Password))
                {
                    requiresAuthentication.Checked = true;
                }
                else
                {
                    requiresAuthentication.Checked = false;
                }
            }
        }

        private void SetAuthenticationControls(object sender, EventArgs args)
        {
            SetAuthenticationControls();
        }

        private void SetAuthenticationControls()
        {
            RunOnUiThread(() => {
                username.Visibility = requiresAuthentication.Checked ? ViewStates.Visible : ViewStates.Invisible;
                password.Visibility = requiresAuthentication.Checked ? ViewStates.Visible : ViewStates.Invisible;
                username_label.Visibility = requiresAuthentication.Checked ? ViewStates.Visible : ViewStates.Invisible;
                password_label.Visibility = requiresAuthentication.Checked ? ViewStates.Visible : ViewStates.Invisible;
            });
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_configure_samba_edit, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_delete)
            {
                Delete();
                Finish();
            }
            else if (id == Resource.Id.action_update)
            {
                Update();
                Finish();
            }

            return base.OnOptionsItemSelected(item);
        }

        private void Delete()
        {
            ConfigurationSubsystem.Instance.DeleteSambaConnectionForId(id);
        }

        private void Update()
        {
            if (!requiresAuthentication.Checked)
            {
                username.Text = null;
                password.EditText.Text = null;
            }

            var myConfigurationSamba = new SambaConnection()
            {
                Id = id,
                Name = name.Text,
                Hostname = hostname.Text,
                IP = ip.Text,
                Sharename = sharename.Text,
                Username = username.Text,
                Password = password.EditText.Text
            };

            ConfigurationSubsystem.Instance.UpdateSambaConnectionForId(myConfigurationSamba);
        }

    }
}