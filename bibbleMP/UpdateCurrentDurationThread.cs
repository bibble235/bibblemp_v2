﻿using System.Threading;
using Android.OS;
using Android.Widget;
using AndroidCommonLib;

namespace bibbleMP
{
    public class UpdateCurrentDurationThread : AsyncTask<string, int, bool>
    {
        public TextView _songTotalDurationLabel;
        public TextView _songCurrentDurationLabel;
        public MusicServiceConnection _musicServiceConnection;
        public bool StopThread = false;
        
        protected override bool RunInBackground(params string[] @params)
        {
            while (!StopThread)
            {
                if (_musicServiceConnection != null)
                {
                    var totalDuration = _musicServiceConnection.Binder.Service.GetDuration();
                    var currentDuration = _musicServiceConnection.Binder.Service.GetCurrentPosition();

                    // Displaying Total Duration time
                    _songTotalDurationLabel.Text = TimerHelpers.MillisecondsToTimer(totalDuration);

                    // Displaying time completed playing
                    _songCurrentDurationLabel.Text = TimerHelpers.MillisecondsToTimer(currentDuration);
                }

                Thread.Sleep(500);
            }
            return true;
        }
    }
}