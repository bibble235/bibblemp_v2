﻿using Android.Views;

namespace bibbleMP
{
    public interface IItemClickListener
    {
        void OnItemClick(View view, int position);
    }
}