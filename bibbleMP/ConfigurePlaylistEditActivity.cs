﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using ConfigurationLib;

namespace bibbleMP
{
    [Activity(Label = "Edit Play List", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class ConfigurePlaylistEditActivity : AppCompatActivity
    {
        private EditText _name;
        private EditText _location;

        private string _id;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


            SetContentView(Resource.Layout.activity_configure_playlist_edit);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar_configure_playlist_edit);
            SetSupportActionBar(toolbar);

            _name = FindViewById<EditText>(Resource.Id.name);
            _location = FindViewById<EditText>(Resource.Id.location);
            
            var myExtras = Intent.Extras;
            _id = myExtras.GetString("id");

            OnSetContent();
        }

        private void OnSetContent()
        {
            var myConfigurationPlaylist = ConfigurationSubsystem.Instance.GetConfigurationPlaylistForId(_id);
            if (myConfigurationPlaylist != null)
            {
                _name.Text = myConfigurationPlaylist.Name;
                _location.Text = myConfigurationPlaylist.Location;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_configure_playlist_edit, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_delete)
            {
                OnDelete();
                Finish();
            }
            else if (id == Resource.Id.action_update)
            {
                OnUpdate();
                Finish();
            }
            else if (id == Resource.Id.action_play)
            {
                Intent intent = new Intent();
                intent.PutExtra(ActivityActions.Id, _id);
                intent.PutExtra(ActivityActions.Action, ActivityActions.Play);
                SetResult(Result.Ok, intent);
                Finish();
            }

            return base.OnOptionsItemSelected(item);
        }

        private void OnDelete()
        {
            ConfigurationSubsystem.Instance.DeleteConfigurationPlaylistConnectionForId(_id);
        }

        private void OnUpdate()
        {
            var myConfigurationPlaylist = new ConfigurePlaylist()
            {
                Id = _id,
                Name = _name.Text,
                Location = _location.Text
            };

            ConfigurationSubsystem.Instance.UpdateConfigurationPlaylistForId(myConfigurationPlaylist);
        }

    }
}