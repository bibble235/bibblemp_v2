﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using ConfigurationLib;

namespace bibbleMP
{
    [Activity(Label = "Play Lists", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class ConfigurePlaylistActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {

        public static int AddFinished = 0xe111;
        public static int UpdateFinished = 0xe112;

        private ListView _listView;
        private List<ConfigurePlaylist> _configurationPlayLists;
        private ConfigurePlaylistAdapter _configurePlaylistAdapter;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_configure_playlist);
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            _listView = FindViewById<ListView>(Resource.Id.listview);
            
            _configurationPlayLists = ConfigurationSubsystem.Instance.GetConfigurationPlaylists();

            _configurePlaylistAdapter = new ConfigurePlaylistAdapter(this,_configurationPlayLists);
            _listView.Adapter = _configurePlaylistAdapter;

            _listView.ItemClick += listView_ItemClick;
            // Create your application here
        }

        private void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var myIntent = new Intent(this,typeof(ConfigurePlaylistEditActivity));
            myIntent.PutExtra("id", _configurePlaylistAdapter[e.Position].Id);
            StartActivityForResult(myIntent, AddFinished);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == AddFinished || requestCode == UpdateFinished)
            {
                _configurePlaylistAdapter.OnRefresh(ConfigurationSubsystem.Instance.GetConfigurationPlaylists());

                var myAction = data?.GetStringExtra(ActivityActions.Action);
                if (!string.IsNullOrEmpty(myAction))
                {
                    if (myAction == ActivityActions.Play)
                    {
                        Intent intent = new Intent();
                        intent.PutExtra(ActivityActions.Action, ActivityActions.Play);
                        intent.PutExtra(ActivityActions.Id, data.GetStringExtra(ActivityActions.Id));
                        SetResult(Result.Ok, intent);
                        Finish();
                    }
                }
            }
        }

        public override void OnBackPressed()
        {

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_configure_playlist, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_add)
            {
                var nextActivity = new Intent(this, typeof(ConfigurePlaylistNewActivity));
                StartActivityForResult(nextActivity, AddFinished);
            }

            return base.OnOptionsItemSelected(item);
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }

    }
}