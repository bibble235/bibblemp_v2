﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using ConfigurationLib;

namespace bibbleMP
{
    [Activity(Label = "New Play List", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class ConfigurePlaylistNewActivity : AppCompatActivity
    {
        private EditText _name;
        private EditText _location;

        public ConfigurePlaylistNewActivity()
        {
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_configure_playlist_new);
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar_configure_playlist_new);
            SetSupportActionBar(toolbar);

            _name = FindViewById<EditText>(Resource.Id.name);
            _location = FindViewById<EditText>(Resource.Id.location);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_configure_playlist_new, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_save)
            {
                OnSave();
                Finish();
            }

            return base.OnOptionsItemSelected(item);
        }

        private void OnSave()
        {
            var myConfigurationPlaylist = new ConfigurePlaylist()
            {
                Id = Guid.NewGuid().ToString(),
                Name = _name.Text,
                Location = _location.Text,
            };

            ConfigurationSubsystem.Instance.AddConfigurationPlaylists(myConfigurationPlaylist);
        }

    }
}