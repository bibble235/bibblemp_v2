﻿using System;
using System.Collections.Generic;
using WindowsMediaPlayerLib;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidCommonLib;
using ConfigurationLib;
using DataLib;
using Java.Lang;
using MusicServiceLib;
using Newtonsoft.Json;
using SambaLib;
using Exception = System.Exception;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace bibbleMP
{
    [Activity(
        Label = "@string/app_name",
        Theme = "@style/AppTheme.NoActionBar",
        MainLauncher = true,
        ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    // ReSharper disable once UnusedMember.Global
    public class MainActivity : AppCompatActivity,
        NavigationView.IOnNavigationItemSelectedListener,
        IItemClickListener,
        SeekBar.IOnSeekBarChangeListener
    {
        static readonly string TAG = typeof(MainActivity).FullName;

        private string _CurrentPlayList;

        private RecyclerView _playListRecyclerView;
        private RecyclerView.LayoutManager _layoutManager;
        private PlayListAdapter _playListAdapter;
        private UpdateRecyclerViewThread _workerThread;

        private MusicServiceConnection _musicConnection;

        private ImageButton _playButton;
        private ImageButton _previousButton;
        private ImageButton _nextButton;
        private ImageButton _backwardButton;
        private ImageButton _forwardButton;

        private int seekForwardTime = 5000;  // 5000 milliseconds
        private int seekBackwardTime = 5000; // 5000 milliseconds

        private SeekBar _progressBar;

        private TextView _songCurrentDurationTextView;
        private TextView _songTotalDurationTextView;

        private int _currentlyPlayingPosition;

        private bool IsPlaying;

        public static int PlaylistFinished = 0x0001;

        private Intent PlayerIntent;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Log.Debug(TAG, "OnCreate");

            Setup();
        }

        /// <summary>
        /// Called by OnCreate and OnRestart
        /// </summary>
        protected override void OnStart()
        {
            base.OnStart();

            Log.Debug(TAG, "OnStart");

            if (_musicConnection == null)
            {
                _musicConnection = new MusicServiceConnection();
            }

            if (PlayerIntent == null)
            {
                PlayerIntent = new Intent(this, typeof(MusicService));
                BindService(PlayerIntent, _musicConnection, Bind.AutoCreate);
                StartService(PlayerIntent);
            }

            if (_CurrentPlayList != null) PlayPlayList();
        }

        /// <summary>
        /// Called by OnStart
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            _CurrentPlayList = savedInstanceState.GetString("Playlist");
            Log.Debug(TAG, $"OnRestore {_CurrentPlayList}");
            base.OnRestoreInstanceState(savedInstanceState);
        }

        /// <summary>
        /// Called by OnRestoreInstanceState
        /// </summary>
        protected override void OnResume()
        {
            base.OnResume();

            Log.Debug(TAG, "OnResume");

            if (_CurrentPlayList != null) PlayPlayList();
        }

        /// <summary>
        /// Called by OnResume
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnSaveInstanceState(Bundle savedInstanceState)
        {
            savedInstanceState.PutString("Playlist", _CurrentPlayList);
            Log.Debug(TAG, $"OnSave {_CurrentPlayList}");

            base.OnSaveInstanceState(savedInstanceState);
        }

        /// <summary>
        /// Called by OnSaveInstanceState
        /// </summary>
        protected override void OnPause()
        {
            base.OnPause();

            Log.Debug(TAG, "OnPause");
        }

        /// <summary>
        /// Called by OnPause
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();

            Log.Debug(TAG, "OnStop");
        }

        /// <summary>
        /// Called by OnStop if restarting
        /// </summary>
        protected override void OnRestart()
        {
            base.OnRestart();

            Log.Debug(TAG, "OnRestart");
        }

        /// <summary>
        /// Called by OnStop if exiting
        /// </summary>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (_musicConnection != null && _musicConnection.IsConnected)
            {
                UnbindService(_musicConnection);
            }

            Log.Debug(TAG, "OnDestroy");
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            Log.Debug(TAG, "OnConfigurationChanged");
        }

        private void OnNextButtonClick(object sender, EventArgs e)
        {
            if (_currentlyPlayingPosition != _playListAdapter.playListTracks.Count - 1)
                _currentlyPlayingPosition = _currentlyPlayingPosition + 1;

            PlayTrack(_currentlyPlayingPosition);
        }

        private void OnBackwardButtonClick(object sender, EventArgs e)
        {
            // Get current song position				
            var currentPosition = _musicConnection.Binder.Service.GetCurrentPosition();

            // Check if seekBackward time is greater than 0 sec
            if (currentPosition - seekBackwardTime >= 0)
            {
                // forward song
                _musicConnection.Binder.Service.SeekTo(currentPosition - seekBackwardTime);
            }
            else
            {
                // backward to starting position
                _musicConnection.Binder.Service.SeekTo(0);
            }
        }

        private void OnForwardButtonClick(object sender, EventArgs e)
        {
            // Get current song position				
            var currentPosition = _musicConnection.Binder.Service.GetCurrentPosition();

            // Check if seekForward time is lesser than song duration
            if (currentPosition + seekForwardTime <= _musicConnection.Binder.Service.GetDuration())
            {
                // Forward song
                _musicConnection.Binder.Service.SeekTo((int)currentPosition + seekForwardTime);
            }
            else
            {
                // Forward to end position
                _musicConnection.Binder.Service.SeekTo(_musicConnection.Binder.Service.GetDuration());
            }
        }

        private void OnPreviousButtonClick(object sender, EventArgs e)
        {
            if (_currentlyPlayingPosition != 0)
                _currentlyPlayingPosition = _currentlyPlayingPosition - 1;

            PlayTrack(_currentlyPlayingPosition);
        }

        private void PlayTrack(int position)
        {
            _musicConnection.Binder.Service.PlayNewSong(position);

            UpdateProgressBar();
            _currentlyPlayingPosition = position;
        }

        private void OnStopButtonClick(object sender, EventArgs e)
        {
            _musicConnection.Binder.Service.OnStop();
        }

        private void OnPlayButtonClick(object sender, EventArgs e)
        {
            if (!IsPlaying)
            {
                IsPlaying = true;
                _musicConnection.Binder.Service.OnPlay();
            }
            else
            {
                IsPlaying = false;
                _musicConnection.Binder.Service.OnPause();
            }

            UpdatePlayButton();
        }

        private void OnPauseButtonClick(object sender, EventArgs e)
        {
            _musicConnection.Binder.Service.OnPause();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == PlaylistFinished)
            {
                var myAction = data?.GetStringExtra(ActivityActions.Action);
                if (!string.IsNullOrEmpty(myAction))
                {
                    if (myAction == ActivityActions.Play)
                    {
                        var myId = data.GetStringExtra(ActivityActions.Id);
                        if (!string.IsNullOrEmpty(myId))
                        {
                            var myPlayList = ConfigurationSubsystem.Instance.GetConfigurationPlaylistForId(myId);
                            HandlePlayPlayList(myPlayList);
                        }
                    }
                }
            }
        }

        private void HandlePlayPlayList(ConfigurePlaylist playList)
        {
            RunOnUiThread(() =>
            {
                var myWindowsPlayListFile = SambaSubsystem.Instance.GetSambaFileAsync(playList.Location).Result;
                _CurrentPlayList = myWindowsPlayListFile.Path;
                PlayPlayList();
            });
        }

        private void PlayPlayList()
        {
            RunOnUiThread(() =>
            {
                try
                {
                    var myTask = WindowsMediaPlayerSubsystem.Instance.GetPlayListTracks(_CurrentPlayList);
                    
                    myTask.Wait();
                    var myResult = myTask.Result;
                    
                    var myPlayListStorage = new PlayListStorage(this);
                    myPlayListStorage.StoreAudio(myResult);

                    _playListAdapter.Refresh(myResult);
                    
                    _workerThread.ClearUpdates();
                    for (var myPosition = 0; myPosition < _playListAdapter.playListTracks.Count; myPosition++)
                    {
                        _workerThread.OnAddUpdate(myPosition, _playListAdapter.playListTracks[myPosition]);
                    }
                }
                catch (AggregateException myAggregateException)
                {
                    // ReSharper disable once ConvertClosureToMethodGroup
                    myAggregateException.Handle(inAggregateException => HandleException(inAggregateException));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            });
        }

        public static bool HandleException(Exception inException)
        {
            var myResult = false;

            /*
            if (inException is KeyVaultErrorException)
            {
                myResult = true;

                vssErrorMessage myErrorMessage = vssErrorSubsystem.Instance.GetMessage(
                    "MTL_KEY_VAULT_EXCEPTION_DETAIL",
                    inException.ToString());
                vssErrorSubsystem.Instance.LogMessage(myErrorMessage);
            }
            else if (inException is AdalServiceException)
            {
                myResult = true;

                vssErrorMessage myErrorMessage = vssErrorSubsystem.Instance.GetMessage(
                    "MTL_ADAL_SERVICE_EXCEPTION_DETAIL",
                    inException.ToString());
                vssErrorSubsystem.Instance.LogMessage(myErrorMessage);
            }
            else if (inException is CryptographicException)
            {
                myResult = true;

                vssErrorMessage myErrorMessage = vssErrorSubsystem.Instance.GetMessage(
                    "MTL_ADAL_SERVICE_EXCEPTION_DETAIL",
                    inException.ToString());
                vssErrorSubsystem.Instance.LogMessage(myErrorMessage);
            }
            else if (inException is NullReferenceException)
            {
                myResult = true;

                vssErrorMessage myErrorMessage = vssErrorSubsystem.Instance.GetMessage(
                    "MTL_NULL_REFERENCE_EXCEPTION_DETAIL",
                    inException.ToString());
                vssErrorSubsystem.Instance.LogMessage(myErrorMessage);
            }
            else
            {
                myResult = true;

                vssErrorMessage myErrorMessage = vssErrorSubsystem.Instance.GetMessage(
                    "MTL_UNKNOWN_SECURITY_EXCEPTION",
                    inException.ToString());
                vssErrorSubsystem.Instance.LogMessage(myErrorMessage);
            }
            */
            return myResult;
        }
        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            if (id == Resource.Id.nav_samba)
            {
                var nextActivity = new Intent(this, typeof(ConfigureSambaActivity));
                StartActivity(nextActivity);
            }
            else if (id == Resource.Id.nav_play_lists)
            {
                var nextActivity = new Intent(this, typeof(ConfigurePlaylistActivity));
                StartActivityForResult(nextActivity, PlaylistFinished);
            }

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }

        public void OnItemClick(View view, int position)
        {
            IsPlaying = true;

            UpdatePlayButton();

            Log.Debug(TAG, $"OnItemClick attempting to play next track at position {position}");

            var myPlayListStorage = new PlayListStorage(this);
            myPlayListStorage.StoreAudioIndex(position);

            if (_musicConnection.IsConnected)
            {
                PlayTrack(position);
            }
        }

        private void Setup()
        {
            SetContentView(Resource.Layout.activity_main);

            var myToolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(myToolbar);

            // Set up Side Drawer
            var myDrawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            var myActionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                myDrawer,
                myToolbar,
                Resource.String.navigation_drawer_open,
                Resource.String.navigation_drawer_close);

            myDrawer.AddDrawerListener(myActionBarDrawerToggle);
            myActionBarDrawerToggle.SyncState();

            var myNavigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            myNavigationView.SetNavigationItemSelectedListener(this);

            // Set up Configuration
            if (ConfigurationSubsystem.Instance == null)
                // ReSharper disable once ObjectCreationAsStatement
                new ConfigurationSubsystem(this);

            // Set up controls
            _previousButton = FindViewById<ImageButton>(Resource.Id.btnPrevious);
            _backwardButton = FindViewById<ImageButton>(Resource.Id.btnBackward);
            _playButton = FindViewById<ImageButton>(Resource.Id.btnPlay);
            _forwardButton = FindViewById<ImageButton>(Resource.Id.btnForward);
            _nextButton = FindViewById<ImageButton>(Resource.Id.btnNext);

            _progressBar = FindViewById<SeekBar>(Resource.Id.songProgressBar);

            _songCurrentDurationTextView = (TextView) FindViewById(Resource.Id.songCurrentDurationLabel);
            _songTotalDurationTextView = (TextView) FindViewById(Resource.Id.songTotalDurationLabel);

            _playListRecyclerView = FindViewById<RecyclerView>(Resource.Id.playlist_recycler_view);

            _playButton.Click += OnPlayButtonClick;
            _backwardButton.Click += OnBackwardButtonClick;
            _forwardButton.Click += OnForwardButtonClick;
            _previousButton.Click += OnPreviousButtonClick;
            _nextButton.Click += OnNextButtonClick;

            // use a linear layout manager
            _layoutManager = new LinearLayoutManager(this);
            _playListRecyclerView.SetLayoutManager(_layoutManager);
            _playListRecyclerView.HasFixedSize = true;

            // Create and attach adapter to view
            _playListAdapter = new PlayListAdapter(new List<PlayListTrack>());
            _playListRecyclerView.SetAdapter(_playListAdapter);

            _playListAdapter.SetClickListener(this);

            _workerThread = (UpdateRecyclerViewThread) new UpdateRecyclerViewThread().Execute();
            _workerThread.PlayListRecyclerView = _playListRecyclerView;
            _workerThread.PlayListAdapter = _playListAdapter;

            _progressBar.SetOnSeekBarChangeListener(this); // Important

            _updateProgressBarRunnable = new Runnable(() =>
            {
                var totalDuration = _musicConnection.Binder.Service.GetDuration();
                var currentDuration = _musicConnection.Binder.Service.GetCurrentPosition();

                // Displaying Total Duration time
                _songTotalDurationTextView.Text = TimerHelpers.MillisecondsToTimer(totalDuration);

                // Displaying time completed playing
                _songCurrentDurationTextView.Text = TimerHelpers.MillisecondsToTimer(currentDuration);

                var progress = (int)(TimerHelpers.GetProgressPercentage(currentDuration, totalDuration));
                _progressBar.Progress = progress;

                // Running this thread after 100 milliseconds
                _updateProgressBarHandler.PostDelayed(_updateProgressBarRunnable, 100);
            });

            /*
            _UpdateNameAndDurationRunnable = new Runnable(() =>
            {
                var totalDuration = _musicConnection.GetDuration();
                var currentDuration = _musicConnection.GetCurrentPosition();

                // Displaying Total Duration time
                _songTotalDurationTextView.Text = TimerHelpers.MillisecondsToTimer(totalDuration);

                // Displaying time completed playing
                _songCurrentDurationTextView.Text = TimerHelpers.MillisecondsToTimer(currentDuration);
            });
            */

        }

        private void UpdatePlayButton()
        {
            _playButton.SetImageResource(
                IsPlaying ? Resource.Drawable.btn_pause : Resource.Drawable.btn_play);
        }

        private Handler _updateProgressBarHandler = new Handler();
        private Runnable _updateProgressBarRunnable;
        
        public void UpdateProgressBar()
        {
            _updateProgressBarHandler.PostDelayed(_updateProgressBarRunnable, 100);
        }

        public void OnProgressChanged(SeekBar seekBar, int progress, bool fromUser)
        {
        }

        public void OnStartTrackingTouch(SeekBar seekBar)
        {
            _updateProgressBarHandler.RemoveCallbacks(_updateProgressBarRunnable);
        }

        public void OnStopTrackingTouch(SeekBar seekBar)
        {
            _updateProgressBarHandler.RemoveCallbacks(_updateProgressBarRunnable);

            var totalDuration = _musicConnection.Binder.Service.GetDuration();
            var currentPosition = TimerHelpers.ProgressToTimer(seekBar.Progress, totalDuration);

            // Seek either forward or backward
            _musicConnection.Binder.Service.SeekTo(currentPosition);

            // Update progress bar
            UpdateProgressBar();
        }
    }

}

