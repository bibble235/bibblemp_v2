﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using ConfigurationLib;

namespace bibbleMP
{
    [Activity(Label = "Samba Connections", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class ConfigureSambaActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {

        public static int ADD_FINISHED = 0xe111;
        public static int UPDATE_FINISHED = 0xe112;

        private ListView listView;
        private List<SambaConnection> sambaConnections;
        private ConfigureSambaAdapter configureSambaAdapter;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_configure_samba);
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            listView = FindViewById<ListView>(Resource.Id.listview);
            
            sambaConnections = ConfigurationSubsystem.Instance.GetSambaConnections();

            configureSambaAdapter = new ConfigureSambaAdapter(this,sambaConnections);
            listView.Adapter = configureSambaAdapter;

            listView.ItemClick += listView_ItemClick;
            // Create your application here
        }

        private void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var myAtPosition = listView.GetItemAtPosition(e.Position);

            var myIntent = new Intent(this,typeof(ConfigureSambaEditActivity));
            myIntent.PutExtra("id", configureSambaAdapter[e.Position].Id);
            StartActivityForResult(myIntent, ADD_FINISHED);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == ADD_FINISHED || requestCode == UPDATE_FINISHED)
            {
                configureSambaAdapter.Refresh(ConfigurationSubsystem.Instance.GetSambaConnections());
            }
        }

        public override void OnBackPressed()
        {

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_configure_samba, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_add_configure_samba)
            {
                var nextActivity = new Intent(this, typeof(ConfigureSambaNewActivity));
                StartActivityForResult(nextActivity, ADD_FINISHED);
            }

            return base.OnOptionsItemSelected(item);
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }

    }
}