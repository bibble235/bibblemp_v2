﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using DataLib;

namespace bibbleMP
{
    public class PlayListViewHolder : RecyclerView.ViewHolder, View.IOnClickListener
    {
        public TextView DisplayNameTextView { get; set; }
        public TextView DisplayLengthTextView { get; set; }

        public IItemClickListener ItemClickListener; 

        public PlayListViewHolder(View itemView, IItemClickListener itemClickListener) : base(itemView)
        {
            DisplayNameTextView = itemView.FindViewById<TextView>(Resource.Id.display_name);
            DisplayLengthTextView = itemView.FindViewById<TextView>(Resource.Id.display_length);

            ItemClickListener = itemClickListener;

            itemView.SetOnClickListener(this);
        }

        public void OnClick(View v)
        {
            ItemClickListener?.OnItemClick(v, LayoutPosition);
        }
    }
}