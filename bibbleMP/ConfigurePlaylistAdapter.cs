﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using ConfigurationLib;

namespace bibbleMP
{
    public class ConfigurePlaylistAdapter : BaseAdapter <ConfigurePlaylist>
    {
        public List<ConfigurePlaylist> ConfigurePlayLists;
        private readonly Context _context;
        public ConfigurePlaylistAdapter(Context context, List<ConfigurePlaylist> configurePlayLists)
        {
            _context = context;
            OnRefresh(configurePlayLists);
        }

        public void OnRefresh(List<ConfigurePlaylist> configurePlayLists)
        {
            ConfigurePlayLists = configurePlayLists.OrderBy(x=>x.Name).ToList();
            NotifyDataSetChanged();
        }
        public override ConfigurePlaylist this[int position] => ConfigurePlayLists[position];

        public override int Count => ConfigurePlayLists.Count;

        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var row = convertView;
            try
            {
                if (row == null)
                {
                    row = LayoutInflater.From(_context).Inflate(Resource.Layout.configure_playlist_row_item, null, false);
                }

                TextView txtName = row.FindViewById<TextView>(Resource.Id.name);
                txtName.Text = ConfigurePlayLists[position].Name;
                TextView txtIp = row.FindViewById<TextView>(Resource.Id.location);
                txtIp.Text = ConfigurePlayLists[position].Location;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return row;
        }
    }
}