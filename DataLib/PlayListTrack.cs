﻿namespace DataLib
{
    public class PlayListTrack
    {
        public enum FileIssuesReasons
        {
            FileUnknown,
            FileOk,
            FileNotFound,
            FileFormatUnsupported,
            FileFormatCorrupt
        }

        public int Postion { get; set; }
        public FileIssuesReasons FileIssues { get; set; }
        public string DisplayName { get; set; }
        public string DisplayLength { get; set; }
        public string Filename { get; set; }
        public int Length { get; set; }
        public string Artist => "FIXME";
        public string Album => "FIXME";
        public string Title => "FIXME";


        public PlayListTrack(string filename)
        {
            Filename = filename;
            var mySplitFilename = filename.Split('\\');
            DisplayName = mySplitFilename[mySplitFilename.Length - 1];
            DisplayLength = "00:00";
            Length = -1;
            FileIssues = FileIssuesReasons.FileUnknown;

        }
    }
}