﻿using System.Collections.Generic;
using Android.Content;
using Newtonsoft.Json;

namespace DataLib
{
    public class PlayListStorage
    {
        private const string STORAGE = " nz.co.bibble.bibblemp.STORAGE";
        private ISharedPreferences preferences;
        private Context context;

        public PlayListStorage(Context context)
        {
            this.context = context;
        }

        public void StoreAudio(IList<PlayListTrack> playList)
        {
            preferences = context.GetSharedPreferences(STORAGE, FileCreationMode.Private);

            ISharedPreferencesEditor editor = preferences.Edit();

            var myJson = JsonConvert.SerializeObject(playList, Formatting.None);
            editor.PutString("audioArrayList", myJson);
            editor.Apply();
        }

        public IList<PlayListTrack> LoadAudio()
        {
            preferences = context.GetSharedPreferences(STORAGE, FileCreationMode.Private);

            var myJson = preferences.GetString("audioArrayList", null);

            var myPlayList = JsonConvert.DeserializeObject<IList<PlayListTrack>>(myJson);

            return myPlayList;
        }

        public void StoreAudioIndex(int index)
        {
            preferences = context.GetSharedPreferences(STORAGE, FileCreationMode.Private);
            ISharedPreferencesEditor editor = preferences.Edit();
            editor.PutInt("audioIndex", index);
            editor.Apply();
        }

        public int LoadAudioIndex()
        {
            preferences = context.GetSharedPreferences(STORAGE, FileCreationMode.Private);
            return preferences.GetInt("audioIndex", -1); //return -1 if no data found
        }

        public void ClearCachedAudioPlaylist()
        {
            preferences = context.GetSharedPreferences(STORAGE, FileCreationMode.Private);
            ISharedPreferencesEditor editor = preferences.Edit();
            editor.Clear();
            editor.Commit();
        }

    }
}
