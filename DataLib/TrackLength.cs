﻿using System;

namespace DataLib
{
    public class TrackLength
    {
        public enum DurationUnits
        {
            Milliseconds,
            Seconds
        }
        public int Length { get;  }
        public string DisplayLength { get; }

        public TrackLength(): this(0, DurationUnits.Seconds)
        {
        }

        public TrackLength(int length, DurationUnits durationUnit)
        {
            TimeSpan myTimeSpan = new TimeSpan();

            Length = durationUnit == DurationUnits.Milliseconds ? length : length * 1000;

            myTimeSpan = TimeSpan.FromMilliseconds(Length);

            DisplayLength = myTimeSpan.Hours != 0 ? 
                $"{myTimeSpan.Hours:D2}:{myTimeSpan.Minutes:D2}:{myTimeSpan.Seconds:D2}" : 
                $"{myTimeSpan.Minutes:D2}:{myTimeSpan.Seconds:D2}";
        }
    }
}